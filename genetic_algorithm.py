###################################
### Genetic algorithm procedure ###
###################################
import numpy as np
import os
import argparse
from specs_genetic import *
from multiprocessing import Pool, cpu_count
from datetime import datetime

# Model argument and simulation parameters
parser = argparse.ArgumentParser(description='Choose which model you want to compute performance for.')
parser.add_argument("model")
args   = parser.parse_args()
plot   = 0                                 # for debugging: plot model behaviour?
nCores = int(nSubjects/2)                  # how many cores are used (1 for no multiprocessing)
nCores = min(max(1, nCores), cpu_count())  # make sure that no irrelevant number is chosen
if plot:
    nCores = 1

# Correct model will be used
if args.model.lower() == 'random':
    from specs_genetic import computeRandomPerformance
if args.model.lower() in ['bouma']:
    from specs_bouma import fitWeightFunction, computeBoumaPerformance
    v_weights, h_weights = fitWeightFunction()
if args.model.lower() in ['popcode', 'popart']:
    from specs_popcode import poolRange, poolThresh, generatePoolMap, computePopCodePerformance
    poolMap = generatePoolMap(poolRange, poolThresh)
if args.model.lower() == 'laminart':
    from specs_bouma import fitWeightFunction
    from specs_laminart import computeLaminartPerformance
    v_weights, h_weights = fitWeightFunction()
if args.model.lower() == 'textures':
    from specs_textures import computeTexturesPerformance
    nCores = 1
if args.model.lower() == 'deepnet':
    from specs_deepnet import AlexNet, get_output_size, Classifier, computeDeepNetPerformance
    use_gpu       =  True
    nCores        =  1
    image_size    = (3, 227, 227)
    readout_layer = [4]
    weights_paths = ['./Deepnet/model_checkpoints/layer_'+str(readout_layer[0])+'_subject_'+str(s)+'.pt' for s in [0]*nSubjects]
    alexnet       =  AlexNet()
    input_size    =  get_output_size(alexnet, readout_layer[0])
    classifiers   = [Classifier(input_size=input_size, weights_path=weights_path) for weights_path in weights_paths]
    if use_gpu and torch.cuda.is_available():
        alexnet     =  alexnet.    to('cuda')
        classifiers = [classifier. to('cuda') for classifier in classifiers]
if args.model.lower() == 'capsules':
    os.environ['KMP_WARNINGS'] = 'off'
    os.environ["TF_CPP_MIN_LOG_LEVEL"] = "1"
    import tensorflow as tf
    tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)
    import sys
    sys.path.append('./Capsules')
    from specs_capsules import computeCapserPerformance
    from capser_model_fn import capser_model_fn, cnn_model_fn
    from parameters import parameters
    model_fn      = capser_model_fn if parameters.net_type.lower() == 'capsnet' else cnn_model_fn
    nCores        = 1
    vdb_targ_pos  = targPos
    caps_targ_pos = parameters.targ_pos
    caps_stim_shp = parameters.stim_shape
    top_pad       = vdb_targ_pos[0] - caps_targ_pos[0]
    left_pad      = vdb_targ_pos[1] - caps_targ_pos[1]
    n_repeats     = nSubjects//len(parameters.winners) + int(nSubjects % len(parameters.winners) != 0)
    logdirs       = ['./Capsules/' + parameters.data_path[2:] + '/' + parameters.model_name + '/%s/' % (i,) for i in parameters.winners*n_repeats]
    capsers       = [tf.estimator.Estimator(model_fn=model_fn, model_dir=l, params={'log_dir': l, 'trials': nTrials}) for l in logdirs]
if args.model.lower() == 'yourmodel':
    from specs_yourmodel import computeYourModelPerformance

# Each subject is one def, to use parallel processing for some models
def runOneSubject(i):
    print('Subject '+str(i+1)+' over '+str(nSubjects)+' running.')
    allPerformances = np.zeros((nSessions, nGenerations))
    
    # Each subject runs several GA sessions
    for session in range(nSessions):
        print('\tSession ' + str(session + 1) + ' over ' + str(nSessions) + ' running.')

        # Initiate the GA procedure by randomly selecting the first generation of displays
        displays    = np.zeros((nGenerations+1, nStartingDisplays, xDim, yDim), dtype=int)  # note: (x,y) = (col,row) so transposed later
        displays[0] = setTheFirstGeneration(nStartingDisplays=nStartingDisplays, xDimension=xDim, yDimension=yDim, initialProp=initialProp[i, session])
        genList     = range(nGenerations)
        
        # Loop over the generations
        for genIndex, generation in enumerate(genList):
            nDisplays   = nStartingDisplays if generation == 0 else nEvolvedDisplays
            performance = np.zeros((nDisplays))

            # Random model performance selection
            if args.model.lower() == 'random':
                for displayIndex in range(nDisplays):
                    performance[displayIndex] = computeRandomPerformance()
                displayList = list(range(nDisplays))
                bestDisp    = [x for _, x in sorted(zip(performance, displayList))][-nParents:]

            # Bouma model performance selection
            elif args.model.lower() == 'bouma':
                for displayIndex in range(nDisplays):
                    performance[displayIndex] = computeBoumaPerformance(displays[generation, displayIndex].T, v_weights, h_weights, doVdB=True, plot=plot)
                displayList = list(range(nDisplays))
                bestDisp    = [x for _, x in sorted(zip(performance, displayList))][-nParents:]

            # Laminart model performance selection
            elif args.model.lower() == 'laminart':
                for displayIndex in range(nDisplays):
                    performance[displayIndex] = computeLaminartPerformance(displays[generation, displayIndex].T, v_weights, h_weights, plot=plot)
                displayList = list(range(nDisplays))
                bestDisp    = [x for _, x in sorted(zip(performance, displayList))][-nParents:]

            # Textures model performance selection
            elif args.model.lower() == 'textures':
                for displayIndex in range(nDisplays):
                    performance[displayIndex] = computeTexturesPerformance(displays[generation, displayIndex].T, plot=plot)
                displayList = list(range(nDisplays))
                bestDisp    = [x for _, x in sorted(zip(performance, displayList))][-nParents:]

            # Population coding model performance selection (new version)
            elif args.model.lower() == 'popcode':
                for displayIndex in range(nDisplays):
                    performance[displayIndex] = computePopCodePerformance(displays[generation, displayIndex].T, poolMap, doSegment=0, plot=plot)
                displayList = list(range(nDisplays))
                bestDisp    = [x for _, x in sorted(zip(performance, displayList))][-nParents:]

            # Laminart model segments stimulus and population coding is the interaction mechanism
            elif args.model.lower() == 'popart':
                for displayIndex in range(nDisplays):
                    performance[displayIndex] = computePopCodePerformance(displays[generation, displayIndex].T, poolMap, doSegment=1, plot=plot)
                displayList = list(range(nDisplays))
                bestDisp    = [x for _, x in sorted(zip(performance, displayList))][-nParents:]

            # Deep neural network performance selection
            elif args.model.lower() == 'deepnet':
                test_batch  = torch.zeros((nTrials*nDisplays,)+image_size)
                test_labels = torch.zeros((nTrials*nDisplays,), dtype=torch.long)
                if use_gpu and torch.cuda.is_available():
                    test_batch  =  test_batch. to('cuda')
                    test_labels =  test_labels.to('cuda')
                for displayIndex in range(nDisplays):
                    test_batch[displayIndex*nTrials:displayIndex*nTrials+nTrials], test_labels[displayIndex*nTrials:displayIndex*nTrials+nTrials] = make_stim_batch(displays[generation, displayIndex].T)
                with torch.no_grad():
                    alexnet.eval()
                    outputs = alexnet(test_batch, readout_layer)[0]
                for displayIndex in range(nDisplays):
                    this_output = outputs[    displayIndex*nTrials:(displayIndex+1)*nTrials]
                    this_label  = test_labels[displayIndex*nTrials:(displayIndex+1)*nTrials]
                    performance[displayIndex] = computeDeepNetPerformance(this_output, this_label, classifiers[i])
                displayList = list(range(nDisplays))
                bestDisp    = [x for _, x in sorted(zip(performance, displayList))][-nParents:]

            # Capsule network performance selection
            elif args.model.lower() == 'capsules':
                for displayIndex in range(nDisplays):
                    this_display = displays[generation, displayIndex].T[top_pad:top_pad + caps_stim_shp[0], left_pad:left_pad + caps_stim_shp[1]]
                    performance[displayIndex] = computeCapserPerformance(capsers[i], this_display, plot=plot)
                displayList = list(range(nDisplays))
                bestDisp    = [x for _, x in sorted(zip(performance, displayList))][-nParents:]

            # Your model performance selection
            elif args.model.lower() == 'yourmodel':
                for displayIndex in range(nDisplays):
                    performance[displayIndex] = computeYourModelPerformance(displays[generation, displayIndex].T)
                displayList = list(range(nDisplays))
                bestDisp    = [x for _, x in sorted(zip(performance, displayList))][-nParents:]

            # Invalid argument
            else:
                raise ValueError('Invalid data type. Use your model name as an argument.')

            # Collect the best displays and make them evolve for the next generation
            displays[generation+1] = evolveTheDisplays(bestDisplays=bestDisp, nParents=nParents, displ=displays[generation], nStartingDisplays=nStartingDisplays, mutationRate=mutationRate, initialProp=int(initialProp[i, session]*100))

            # Monitor performance to make sure it is improving with the generations
            print('\t\tGeneration ' + str(genIndex+1) + ' - perf: %.2f' % (performance.mean(),))
            allPerformances[session, genIndex] = performance.mean()

        # Write the simulation results
        if not os.path.exists('./Raw_Results'):
            os.mkdir('./Raw_Results')
        directory = './Raw_Results/'+args.model.lower()
        if not os.path.exists(directory):
            os.mkdir(directory)
        displays.dump('%s/subj_%i%02i_display_sim' % (directory, session+1, i))

    # Save performance results in .npy arrays
    np.save(directory + '/perf_' + str(i) + '.npy', np.array(allPerformances).mean(axis=0))

# Run the genetic algorithm for all subjects
if __name__ == '__main__':

    # This code uses parallel processing (nCores = 1 to not use it)
    startTime = datetime.now()
    print('The script will use ' + str(nCores) + ' cores to run.')
    if nCores > 1:
        pool = Pool(nCores)                        # Pool(n) to use n cores ; default is max number of cores
        pool.map(runOneSubject, range(nSubjects))  # Process the iterable using multiple cores
    else:
        for i in range(nSubjects):
            runOneSubject(i)
    print('Time to run the script: '+str(datetime.now()-startTime))
    exit()