import matplotlib.pyplot as plt
import matplotlib.gridspec as gspc
import pandas as pd
import numpy as np
import os

# Initialization and main loop
use_practice = False
proportionsV = [0, 20, 40, 60, 80, 100]
insert_in_big_fig = False
file_names = [f for f in os.listdir('./') if '.csv' in f and 'defaultlog' not in f and '3.csv' not in f]
n_subjects = len(file_names)
all_accuracies = np.zeros((n_subjects, len(proportionsV)))
for subject, file_name in enumerate(file_names):

	# Load data file
	data = pd.read_csv('./%s' % (file_name,))
	results = {p: [] for p in proportionsV}
	blocks = data['block']
	props = data['propVertical']
	hits = data['correct']
	for i, (b, p, h) in enumerate(zip(blocks, props, hits)):
		if (b > 1 and not use_practice) or use_practice:
			results[p].append(h)

	# Read accuracy from the data file
	accuracies = []
	print('Subject %s' %(subject))
	for p in results.keys():
		r = results[p]
		acc = sum(r)/len(r)
		print('\tPropV: %3i, acc: %f' % (p, acc))
		accuracies.append(acc)
	all_accuracies[subject] = np.array(accuracies)

# Prepare data and figure
plot_accuracies = all_accuracies.mean(axis=0)
plot_std_devs = all_accuracies.std(axis=0)
if insert_in_big_fig:
	plt.figure(figsize=(6, 3), dpi=300.0) 
	gs = gspc.GridSpec(3, 4)
	gs.update(wspace=0.1, hspace=0.1)
	ax = plt.subplot(gs[1])
	size_factor = 1.0
else:
	size_factor = 3.5
print('Prop. Vert: ', *proportionsV, sep=', ')
print('Accuracies: ', *plot_accuracies, sep=', ')
print('Stdr. Devs: ', *plot_std_devs, sep=', ')

# Plot things
plt.errorbar([p/100.0 for p in proportionsV], plot_accuracies, plot_std_devs/2.0,
	fmt='bo', ms=2.0*size_factor, lw=0.6*size_factor)
plt.axhline(y=0.5, color='#555555', lw=0.6*size_factor, ls='--')
plt.xticks([0.0, 0.2, 0.4, 0.6, 0.8, 1.0], fontsize=4*size_factor)
plt.yticks([0.5, 0.6, 0.7, 0.8, 0.9, 1.0], fontsize=4*size_factor)
plt.xlabel('Proportion of vertical flankers', fontsize=4*size_factor)
plt.ylabel('Accuracy', fontsize=4*size_factor)
x1, x2, _, _ = plt.axis()
plt.axis((x1, x2, 0.47, 1.0))
plt.text(0.65, 0.85, 'N = %i' % (n_subjects,), fontsize=4*size_factor)
plt.savefig('Results_all.png')
plt.show()