# Shrinking Bouma's window

Code for the paper "Shrinking Bouma's window: crowding in dense displays"

## Installation

Install the general requirements and the specific requirements of the model(s) you want to run (see requirements below)

Possible model names: bouma, capsules, deepnet, laminart, popart, popcode, textures, yourmodel

To test your own model, edit the file "specs_yourmodel.py". The model has to output target discrimination accuracy (between 0.0 and 1.0). Specific instructions are given in the comments of "specs_yourmodel.py"

## Usage

* Sparse display measure
```bash
python measure_bouma.py <modelname>
```

* Proportion measure
```bash
python measure_propo.py <modelname>
```

* GA measures (performance and preference measures)
```bash
python genetic_algorithm.py <modelname> # to generate raw GA result files
python measure_genetic.py <modelname> # for the actual measures
```

## Requirements

* General requirements (example with conda)
```bash
conda create **name shrinking_boumas_window python=3.6
conda activate shrinking_boumas_window
conda install numpy, scipy, matplotlib, seaborn, mne, imageio
```

* Bouma model requirements (modelname is bouma or burga)
> No additional requirement

* Capsule network requirements (modelname is capsules)
```bash
# if you don't use a gpu
conda install tensorflow==1.15
# if you use a gpu
conda install tensorflow-gpu==1.15
```

* CNN classifier requirements (modelname is deepnet)
```bash
conda install pytorch==1.2.0 torchvision==0.4.0 cudatoolkit=10.0 -c pytorch
```

* Laminart model requirements (modelname is laminart)
> No additional requirement

* Population coding model requirements (modelname is popcode)
> No additional requirement

* Popart model requirements (modelname is popart)
> No additional requirement

* Texture model requirements (modelname is textures)
```bash
# As administrator
cd <matlabroot>\extern\engines\python
python setup.py install
```