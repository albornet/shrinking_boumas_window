##########################
### Alexnet classifier ###
##########################
import torch
import torch.nn as nn
import torch.utils.model_zoo as model_zoo
import numpy as np
import functools
from specs_genetic import *
from scipy import ndimage

# Compute feed-forward CNN classifier performance
def computeDeepNetPerformance(inputs, labels, model, nTrials=nTrials):

    with torch.no_grad():
        model.eval()
        outputs = model(inputs)
        correct = (outputs.argmax(1) == labels).sum()

        perf = correct.item()/nTrials
        return perf


# AlexNet network
class AlexNet(nn.Module):

    def __init__(self):

        super(AlexNet, self).__init__()
        
        self.features = nn.Sequential(
            nn.Conv2d(3, 64, kernel_size=11, stride=4, padding=2),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=3, stride=2),
            nn.Conv2d(64, 192, kernel_size=5, padding=2),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=3, stride=2),
            nn.Conv2d(192, 384, kernel_size=3, padding=1),
            nn.ReLU(inplace=True),
            nn.Conv2d(384, 256, kernel_size=3, padding=1),
            nn.ReLU(inplace=True),
            nn.Conv2d(256, 256, kernel_size=3, padding=1),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=3, stride=2))

        self.classifier = nn.Sequential(
            nn.Dropout(),
            nn.Linear(256*6*6, 4096),
            nn.ReLU(inplace=True),
            nn.Dropout(),
            nn.Linear(4096, 4096),
            nn.ReLU(inplace=True),
            nn.Linear(4096, 1000))

        self.load_state_dict(model_zoo.load_url('https://download.pytorch.org/models/alexnet-owt-4df8aa71.pth'))
        for p in self.parameters():
            p.requires_grad = False

    def forward(self, x, ns):

        ys = []
        
        for n, layer in enumerate(self.features):
            x = layer(x)
            if n in ns:
                ys.append(x)
        
        x = x.view(x.size(0), functools.reduce(lambda a, b: a*b, x.size()[1:]))
        for n, layer in enumerate(self.classifier):
            x = layer(x)
            if n+len(self.features) in ns:
                ys.append(x)

        return ys


# Classifier without any hidden layer
class Classifier(nn.Module):

    def __init__(self, input_size, weights_path):
        
        super(Classifier, self).__init__()

        n_hidden  = 64
        input_len = functools.reduce(lambda a, b: a*b, input_size)
        self.input_size       = input_size
        self.input_len        = input_len
        self.late_classifiers = nn.Sequential(
            nn.BatchNorm1d(input_len),
            nn.Dropout(),
            nn.Linear(input_len, n_hidden),
            nn.ELU(inplace=True),
            nn.Linear(n_hidden, 2),
            nn.Softmax())
        self.load_state_dict(torch.load(weights_path, map_location=torch.device('cpu')))

    def forward(self, x):

        x = x.view(x.size(0), -1)  # functools.reduce(lambda a, b: a*b, x.size()[1:]))
        return self.late_classifiers(x)


# Give the size of a layer of alexnet given input size
def get_output_size(model, n, input_size=(3, 227, 227)):
    
    output_sizes = []
    x = torch.zeros(input_size).unsqueeze_(dim=0)
    for layer in list(model.features):
        x = layer(x)
        output_sizes.append(x.size()[1:])

    x = x.view(x.size(0), functools.reduce(lambda a, b: a*b, x.size()[1:]))
    for layer in list(model.classifier):
        x = layer(x)
        output_sizes.append(x.size()[1:])

    return output_sizes[n]


# Create patch with defined orientation
def create_patch(ori, patch_size):  
    space = 1
    patch = np.zeros((patch_size, patch_size))
    patch[space:-1-(space-1), (patch_size-1)//2] = 1.0
    patch = ndimage.rotate(patch, ori, order=1, reshape=False)
    return np.dstack([patch]*3)


# Generate a batch of displays that will be fed to alexnet to compute the performance of the DNN
def make_stim_batch(stim, image_size=(3,227,227), noise_level=0.02, patch_size=11, nTrials=nTrials):  # larger noise level?

    data  = torch.zeros((nTrials,) + image_size)
    label = torch.zeros((nTrials,), dtype=torch.long)
    stimulus = np.zeros(tuple([s*patch_size for s in stim.shape]) + (3,))
    for n in range(nTrials):

        targ_ori = np.random.RandomState().choice([-1, 1])*(5.0 if np.abs(stim).sum() < 100 else 7.0)
        label[n] = (1-np.sign(targ_ori))/2  # "\" --> 0 ; "/" --> 1
        for i in range(stim.shape[0]):
            for j in range(stim.shape[1]):
                if stim[i,j] != 0:
                    flank_ori = 0 if stim[i,j] == 1 else 90
                    stimulus[i*patch_size:(i+1)*patch_size, j*patch_size:(j+1)*patch_size, :] = create_patch(flank_ori, patch_size)

        stimulus[targPos[0]*patch_size:(targPos[0]+1)*patch_size, targPos[1]*patch_size:(targPos[1]+1)*patch_size, :] = create_patch(targ_ori, patch_size)
        firstRow = int((image_size[1]-stimulus.shape[0])/2)
        firstCol = int((image_size[2]-stimulus.shape[1])/2)
        data[n, :, firstRow:firstRow+stimulus.shape[0], firstCol:firstCol+stimulus.shape[1]] = torch.tensor(stimulus).permute(2,0,1)

    added_noise = torch.empty((nTrials,)+image_size[-2:]).normal_(0, noise_level)
    for channel in range(data.size()[1]):
        data[:, channel, :, :] += 1.0*added_noise
    data[:] = (data-data.min())/(data.max()-data.min())
    
    return data, label