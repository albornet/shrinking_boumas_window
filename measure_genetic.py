###########################################
### Preference and performance measures ###
###########################################
import matplotlib.pyplot as plt
import numpy as np
import argparse
import seaborn as sns
import mne
import os
from scipy import stats, ndimage
from shutil import copy
from specs_genetic import *

# Function arguments
parser = argparse.ArgumentParser(description='Choose which model you want to compute performance for.')
parser.add_argument('model')
args       =  parser.parse_args()
modlFolder = 'Raw_Results/'+args.model.lower()
randFolder = 'Raw_Results/'+'random'
outFolder  = 'Final_Results/'+args.model.lower()

# Find out the correct folder name
if not os.path.exists('Final_Results/'):
    os.mkdir('Final_Results/')
if not os.path.exists(outFolder):
    os.mkdir(outFolder)
n = 0
while os.path.exists('%s/run_%i' % (outFolder, n)):
    n += 1
outFolder = '%s/run_%i' % (outFolder, n)
os.mkdir(outFolder)

# Copy the model parameters (specs files) inside
if args.model.lower() != 'human':
    spec_name = args.model.lower()
    if spec_name == 'popart':
        spec_name = 'popcode'
    copy('./specs_%s.py' % (spec_name), outFolder)
    copy('./specs_%s.py' % ('genetic'), outFolder)

# Plot performance against generations
perfs = np.zeros((nSubjects, nGenerations))
for i in range(nSubjects):
    perfs[i, :] = np.load(modlFolder + '/perf_' + str(i) + '.npy')
fig, ax = plt.subplots()
ax.errorbar([g+1 for g in range(nGenerations)], perfs.mean(axis=0), yerr=perfs.std(axis=0), fmt='bo')
ax.set_xlim([0.50, nGenerations+0.50])
ax.set_ylim([0.40,              1.00])
fig.savefig('%s/performance' % (outFolder,))
plt.show()

# Go through the data
fileNames  = []
subjects   = range(nSubjects)
modl_disp  = np.zeros((len(subjects),xDim,yDim))
rand_disp  = np.zeros((len(subjects),xDim,yDim))
for generation in range(nGenerations):
    for i in range(nSubjects):
        
        # Create proportion matricces from the simulation results
        n_displays = nStartingDisplays if generation == 0 else nEvolvedDisplays
        for session in range(nSessions):
            modl_tmp      = np.load('%s/subj_%i%02i_display_sim' % (modlFolder, session+1, i), allow_pickle=True)
            rand_tmp      = np.load('%s/subj_%i%02i_display_sim' % (randFolder, session+1, i), allow_pickle=True)
            modl_disp[i] += modl_tmp[generation, 0:n_displays].mean(axis=0)       
            rand_disp[i] += rand_tmp[generation, 0:n_displays].mean(axis=0)

    # Statistics arrays initialization
    finalDisp  = np.zeros((xDim,yDim))
    pValues    = np.zeros((xDim,yDim))
    dataPoint  = np.zeros((len(subjects)))
    dataPoint2 = np.zeros((len(subjects)))

    # Student tests between 0-th and n-th generation H/V proportions
    data_disp = (modl_disp-rand_disp.mean())/(4*nSessions)
    data_disp[:, targPos[0], targPos[1]] = 0
    for x in range(0,xDim):
        for y in range(0,yDim):
            for s in range(len(subjects)):
                dataPoint[s] = data_disp[s,x,y]
            if np.abs(dataPoint).sum() + np.abs(dataPoint.sum()) > 0:
                t, p = stats.ttest_ind(dataPoint, dataPoint2)
                pValues[  x,y] = p if p == p else 1.0  # added a Nan check
                finalDisp[x,y] = data_disp[:,x,y].mean()

    # False discovery rate analysis
    level       = 0.05
    _, pval_fdr = mne.stats.fdr_correction(pValues, alpha=level, method='indep')
    stat        = "standard"
    # stat      = "fdr"
    for x in range(xDim):
        for y in range(yDim):
            if stat == "fdr":
                if pval_fdr[x,y] > level:
                    finalDisp[x,y] = 0
            if stat == "standard":
                if pValues[x,y] > level:
                    finalDisp[x,y] = 0

    # Plot the final test for this generation and save the image
    fig = plt.figure()
    sns.heatmap(np.flipud(finalDisp.transpose()), center=0, cbar=True, xticklabels=False, yticklabels=False, vmin=-1.0, vmax=1.0)
    plt.axis('equal')
    plt.title('Generation %i' % (generation+1,))
    fileNames.append('%s/generation%i.png' % (outFolder, generation+1))
    fig.savefig(fileNames[-1], bbox_inches="tight", pad_inches=0, facecolor='Black')
    plt.show()

# Create the gif and save it
import imageio
images = []
for fileName in fileNames:
    if fileName != fileNames[-1]:
        images.append(imageio.imread(fileName))
    else:
        images.extend([imageio.imread(fileName)]*4)
imageio.mimsave('%s/generations.gif' % (outFolder,), images, duration=0.5)