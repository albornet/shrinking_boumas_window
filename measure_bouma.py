#####################
### Bouma measure ###
#####################
import numpy as np
import argparse
import os
import matplotlib.pyplot as plt
from specs_genetic import *

# Model argument and simulation parameters
parser    = argparse.ArgumentParser(description='Choose which model you want to compute performance for.')
parser.add_argument("model")
args      = parser.parse_args()
plot      = 0                # for debugging: plot details about model behaviour?
dist_max  = 8                # in slots (1 slot = 0.75 deg)
distances = range(dist_max)  # where 0 means no flanker
nSubjects = 100
nTrialsBouma = 12

# Will write in the folder of the last analysed GA procedure
outFolder = 'Final_Results/'+args.model.lower()
if not os.path.exists(outFolder):
    os.mkdir(outFolder)
n = 0
while os.path.exists('%s/run_%i' % (outFolder, n)):
    n += 1
outFolder = '%s/run_%i' % (outFolder, n-1)
if not os.path.exists(outFolder):
    os.mkdir(outFolder)

# Find out the correct name for the output result file
outFileName = outFolder+'/bouma'
n = 1
while os.path.exists(outFileName+'.png'):
    l = len(str(n))
    n += 1
    if outFileName[-l:] == str(n-1):
        outFileName = outFileName[:-l] + str(n)
    else:
        outFileName += str(n)

# Correct model will be used
if args.model.lower() == 'random':
    from specs_genetic import computeRandomPerformance
if args.model.lower() in ['bouma']:
    from specs_bouma import fitWeightFunction, computeBoumaPerformance
    v_weights, h_weights = fitWeightFunction()
if args.model.lower() in ['popcode', 'popart']:
    from specs_popcode import poolRange, poolThresh, generatePoolMap, computePopCodePerformance
    poolMap = generatePoolMap(poolRange, poolThresh)
if args.model.lower() == 'laminart':
    from specs_bouma import fitWeightFunction
    from specs_laminart import computeLaminartPerformance
    v_weights, h_weights = fitWeightFunction()
if args.model.lower() == 'textures':
    from specs_textures import computeTexturesPerformance
if args.model.lower() == 'deepnet':
    import torch
    from specs_deepnet import AlexNet, get_output_size, Classifier, computeDeepNetPerformance, make_stim_batch
    use_gpu       =  True
    image_size    = (3, 227, 227)
    readout_layer = [4]
    weights_paths = ['./Deepnet/model_checkpoints/layer_'+str(readout_layer[0])+'_subject_'+str(s)+'.pt' for s in [0]*nSubjects]
    alexnet       =  AlexNet()
    input_size    =  get_output_size(alexnet, readout_layer[0])
    classifiers   = [Classifier(input_size=input_size, weights_path=weights_path) for weights_path in weights_paths]
    if use_gpu and torch.cuda.is_available():
        alexnet     =  alexnet.    to('cuda')
        classifiers = [classifier. to('cuda') for classifier in classifiers]
if args.model.lower() == 'capsules':
    os.environ['KMP_WARNINGS'] = 'off'
    os.environ["TF_CPP_MIN_LOG_LEVEL"] = "1"
    import tensorflow as tf
    tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)
    import sys
    sys.path.append('./Capsules')
    from specs_capsules import computeCapserPerformance
    from capser_model_fn import capser_model_fn, cnn_model_fn
    from parameters import parameters
    model_fn      = capser_model_fn if parameters.net_type.lower() == 'capsnet' else cnn_model_fn
    nCores        =  1
    vdb_targ_pos  = targPos
    caps_targ_pos = parameters.targ_pos
    caps_stim_shp = parameters.stim_shape
    top_pad       = vdb_targ_pos[0] - caps_targ_pos[0]
    left_pad      = vdb_targ_pos[1] - caps_targ_pos[1]
    n_repeats     = nSubjects//len(parameters.winners) + int(nSubjects % len(parameters.winners) != 0)
    logdirs       = ['./Capsules/' + parameters.data_path[2:] + '/' + parameters.model_name + '/%s/' % (i,) for i in parameters.winners*n_repeats]
    capsers       = [tf.estimator.Estimator(model_fn=model_fn, model_dir=l, params={'log_dir': l, 'trials': nTrials}) for l in logdirs]
if args.model.lower() == 'yourmodel':
    from specs_yourmodel import computeYourModelPerformance

# Simulate the model with sparse displays
results = {'vert': {'plot': '^r', 'perf': {}}, 'hori': {'plot': 'ok', 'perf': {}}}
for d in distances:
    print('Target-flanker distance = %i slots' % (d) if d > 0 else 'Unflanked')
    for oriType in ['vert', 'hori']:
        if d > 0 or oriType == 'vert':
            oriIndex = 1 if oriType == 'vert' else -1
            display  = (np.zeros((yDim, xDim))).astype(int)  # no element
            if d > 0:
                display[targPos[0] + d, targPos[1]    ] = oriIndex
                display[targPos[0] - d, targPos[1]    ] = oriIndex
                display[targPos[0]    , targPos[1] + d] = oriIndex
                display[targPos[0]    , targPos[1] - d] = oriIndex
        
            perf = np.zeros((nSubjects))
            for s in range(nSubjects):
                if args.model.lower() == 'bouma':
                    perf[s] = computeBoumaPerformance(display, v_weights, h_weights, doVdB=True, plot=plot, nTrials=nTrialsBouma)
                elif args.model.lower() == 'laminart':
                    perf[s] = computeLaminartPerformance(display, v_weights, h_weights, plot=plot, nTrials=nTrialsBouma)
                elif args.model.lower() == 'textures':
                    perf[s] = computeTexturesPerformance(display, plot=plot, nTrials=nTrialsBouma)
                elif args.model.lower() == 'popcode':
                    perf[s] = computePopCodePerformance(display, poolMap, doSegment=0, plot=plot, nTrials=nTrialsBouma)
                elif args.model.lower() == 'popart':
                    perf[s] = computePopCodePerformance(display, poolMap, doSegment=1, plot=plot, nTrials=nTrialsBouma)
                elif args.model.lower() == 'deepnet':
                    test_batch, test_labels = make_stim_batch(display, nTrials=nTrialsBouma)
                    if use_gpu and torch.cuda.is_available():
                        test_batch  =  test_batch. to('cuda')
                        test_labels =  test_labels.to('cuda')
                    with torch.no_grad():
                        alexnet.eval()
                        outputs = alexnet(test_batch, readout_layer)[0]
                        perf[s] = computeDeepNetPerformance(outputs, test_labels, classifiers[s], nTrials=nTrialsBouma)
                elif args.model.lower() == 'capsules':
                    this_display = display[top_pad:top_pad + caps_stim_shp[0], left_pad:left_pad + caps_stim_shp[1]]
                    perf[s] = computeCapserPerformance(capsers[s], this_display, plot=plot, nTrials=nTrialsBouma)
                elif args.model.lower() == 'yourmodel':
                    perf[s] = computeYourModelPerformance(display, nTrials=nTrialsBouma)
                else:
                    raise NameError('Are you sure about the model name?')

            if d > 0:
                print('\tType = %s, perf = %3.2f, stdev = %3.2f' % (oriType, perf.mean(), perf.std()))
            else:
                print('\tPerformance = %3.2f, stdev = %3.2f' % (perf.mean(), perf.std()))
            results[oriType]['perf'][d] = {'mean': perf.mean(), 'stdv': perf.std()/np.sqrt(nSubjects)}
        else:
            results[oriType]['perf'][d] = {'mean': 0.0, 'stdv': 1.0}

# Create and save the figure
fig, ax = plt.subplots()
for k in results.keys():
    plot_dist = [d for d in results[k]['perf'].keys()][1:]
    plot_perf = [results[k]['perf'][d]['mean'] for d in plot_dist]
    plot_stdv = [results[k]['perf'][d]['stdv'] for d in plot_dist]
    ax.errorbar([d*slotSize for d in plot_dist], plot_perf, yerr=plot_stdv, fmt=results[k]['plot'], ms=8.0, label=k)

np.save(outFileName+'.npy', results)  # retrieved with np.load(outFileName).item()
ax.legend()
unflk_mean = results['vert']['perf'][0]['mean']
unflk_stdv = results['vert']['perf'][0]['stdv']
ax.axhspan(ymin=unflk_mean-unflk_stdv, ymax=unflk_mean+unflk_stdv, alpha=0.5, color='k')
ax.axhline(y=unflk_mean-unflk_stdv, color='k')
ax.axhline(y=unflk_mean+unflk_stdv, color='k')
ax.set_xlim([0.0, dist_max*slotSize])
ax.set_ylim([0.4,          1.0     ])
fig.savefig(outFileName)
# plt.show()