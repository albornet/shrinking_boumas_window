######################
### Laminart model ###
######################
import numpy as np
from specs_genetic import *

unflankedPerf = 0.96
def computeLaminartPerformance(stim, v_weights, h_weights, plot=False, nTrials=nTrials):

    # Parameters
    barSz     = 5                      # 0.43 deg vs 0.09 deg; odd number only (so that bars are centered in the patches)
    gapSz     = 2                      # 0.16 deg on top and below each bar; any number
    pchSz     = 2*gapSz+barSz          # how many pixel per patch
    segType   = 'around'               # around or uniform
    nSegLayrs = 5                      # number of segmentation signals is nSegLayrs-1
    pchPerDeg = 1.0/slotSize           # how many patch per degree
    pixPerDeg = pchSz*pchPerDeg        # how many pixel per degree
    bouRadius = 0.5*targEcc*pchPerDeg  # Bouma's radius (half the eccentricity) in patches
    segRadius = 12

    # Parameters to map model results to performance
    unflankedPerf    = 0.96
    interferenceGain = 1.0  # 0.6

    # Compute the groups from the stimulus array
    stim[targPos[0], targPos[1]] = 1   # target is almost vertical ("1")
    groupList = groupStim(stim)
    stimImage, groupImage = createImages(stim, groupList, targPos, barSz, gapSz, pchSz, plot=False)

    # Send segmentation signals for each trial and compute crowding
    nCorrects = 0
    for n in range(nTrials):

        # Segment groups into the segmentation layers
        signalsMap, segLists = segmentStim(groupList, groupImage, stimImage, nSegLayrs, targPos, pchSz, bouRadius, segRadius, segType)

        # Add weighted elements within an interaction range, in the target's segmentation layer   
        interference = 0.0
        for h in range(segLists.shape[0]):
            if segLists[h, targPos[0], targPos[1]] != -1:
                for i in range(yDim):
                    for j in range(xDim):

                        # Check whether there was something at position (i,j) and add interaction
                        if stim[i,j] in [1,-1] and not (i == targPos[0] and j == targPos[1]):
                            squaredDistance = ((i-targPos[0])**2 + (j-targPos[1])**2)
                            if segLists[h,i,j] != -1 and not (i,j) == targPos:

                                # Different weights for vertical or horizontal flankers
                                if stim[i,j] == 1:
                                    interference += interferenceGain*v_weights[i,j]  
                                else:
                                    interference += interferenceGain*h_weights[i,j]

        # Compute probability of being correct and roll a dice
        pCorrect   = max(unflankedPerf*(1-interference), 0.5)
        nCorrects += np.random.RandomState().choice(a=[1, 0], p=[pCorrect, 1-pCorrect])  # correct: 1 ; wrong: 0

        # Plot (or not) the segmentation output
        if plot:
            import matplotlib.pyplot as plt
            for h in range(nSegLayrs):
                _, segImage = createImages(stim, segLists[h], targPos, barSz, gapSz, pchSz, plot=False)
                if h == 0:
                    segImage += signalsMap
                plt.subplot(1, nSegLayrs, h+1)
                plt.imshow(segImage.astype(int).clip(min=0, max=255))
            plt.suptitle('Interference: ' + str(interference) + ' ; Perf: ' + str(pCorrect))
            plt.show()

    # Return performance mean to the computer
    return nCorrects/nTrials
    

def groupStim(stim):

    # Pad the stimulus, and initialize the group array
    stimDims  = stim.shape
    groupRng  = 5
    stim      = np.pad(stim, groupRng, 'constant', constant_values=0)
    groupList = np.zeros(stimDims)
    groupList = np.pad(groupList, groupRng, 'constant', constant_values=int(-1))

    # Goe through all existing elements of the stimulus and form the groups
    for row in range(1, stimDims[0] + groupRng):
        for col in range(1, stimDims[1] + groupRng):
            if stim[row, col] != 0:  # 0 means no element at that position

                # A new group is formed if the existing element does not belong to any group
                if groupList[row, col] == 0 and stim[row, col] != 0:
                    groupList[row, col] = np.max(groupList) + 1

                # All possible neighbours that could group with the current element
                shifts = list(range(1, groupRng + 1)[::-1]) + [-r for r in range(1, groupRng + 1)]
                indexList = [(row + i, col) for i in shifts] + [(row, col + j) for j in shifts]
                elemList = [stim[row + i, col] for i in shifts] + [stim[row, col + j] for j in shifts]

                # Go through all identical neighbour elements
                for index, elem in zip(indexList, elemList):
                    if elem == stim[row, col]:

                        # If a different element lies between the current and the neighbour element, no grouping
                        check = True
                        for r in range(min(row, index[0]) + 1, max(row, index[0])):
                            if stim[r, col] == -stim[row, col]:
                                check = False
                        for c in range(min(col, index[1]) + 1, max(col, index[1])):
                            if stim[row, c] == -stim[row, col]:
                                check = False

                        # Group only if allowed
                        if check:

                            # Probability to group decreases with distance (1 for distance = 1)
                            alpha     = 0.2
                            distance  = np.sqrt((row - index[0]) ** 2 + (col - index[1]) ** 2)
                            groupProb = np.exp(1.0 - distance) / alpha

                            # Deal with probability of grouping
                            if np.random.RandomState().uniform() < groupProb:

                                # If the neighbour is not in a group, it joins the current one
                                if groupList[index] == 0:
                                    groupList[index] = groupList[row, col]

                                # If it is already in a group, its entire group and the current group are merged
                                if groupList[index] > 0:  # < groupList[row,col]:
                                    groupList[groupList == groupList[row, col]] = groupList[index]

    # Return the group list to the program
    return groupList[groupRng:-groupRng, groupRng:-groupRng].astype(int)


def createSpacedColors(n):

    # Avoid black, and generate equally spaced colours (n = 7 is the only nice one)
    n        += 1
    max_value = 16581375  # 255**3
    interval  = int(max_value / n)
    colors    = [hex(I)[2:].zfill(6) for I in range(0, max_value, interval)]

    # Return the list of colours to the program
    return [(int(i[:2], 16), int(i[2:4], 16), int(i[4:], 16)) for i in colors[1:]]


def createPatches(barSz, gapSz, pchSz):

    # Initialize vertical, horizontal, empty and target patches
    ones = np.row_stack(gapSz*(0, ) + barSz*(1,) + gapSz*(0,))
    zero = np.row_stack(pchSz*(0,))
    targ = np.row_stack(gapSz*(0, ) + (1,) + (barSz-2)*(0,) + (1,) + gapSz*(0,))
    voidPatch = np.column_stack(pchSz  *(zero,  )                                  )
    vertPatch = np.column_stack(pchSz//2*(zero,  ) + 1*(ones,  ) + pchSz//2*(zero,  ))
    horiPatch = np.row_stack(   pchSz//2*(zero.T,) + 1*(ones.T,) + pchSz//2*(zero.T,))
    targPatch = vertPatch.copy()
    targPatch[:, pchSz//2 - 1] = np.squeeze(targ)
    targPatch[:, pchSz//2 + 1] = np.squeeze(targ)

    # Return the patches to the program (0=void, 1=vertical, 2=target, -1=horizontal)
    return [voidPatch, vertPatch, targPatch, horiPatch]


def createImages(stim, groupList, targetPos, barSz, gapSz, pchSz, plot):

    # Generate a list of coulours to plot the different groups
    stimDims  = stim.shape
    nColours  = 7  # other numbers are less good
    myColours = np.array(createSpacedColors(nColours))

    # Initialize all patches and the final images to plot (group and stimulus)
    stimPatches = createPatches(barSz, gapSz, pchSz)
    stimImage   = np.zeros((stimDims[0] * pchSz, stimDims[1] * pchSz))
    groupImage  = np.zeros((stimDims[0] * pchSz, stimDims[1] * pchSz, 3))

    # Go through all the slots to plot the patches
    for i in range(stimDims[0]):
        for j in range(stimDims[1]):

            # Plot a patch only if there is something in it ("-1" here is "empty", contrary to stim)
            if groupList[i, j] != -1:

                # Plot the patches with correct colours and orientations
                thisPatch = stimPatches[stim[i, j]].copy()
                if i == targetPos[0] and j == targetPos[1]:
                    thisPatch = stimPatches[2]  # "3" is the code for target patch
                thisColour    = myColours[groupList[i, j] % nColours]
                colouredPatch = np.array(
                    [thisPatch*thisColour[0], thisPatch*thisColour[1], thisPatch*thisColour[2]])
                colouredPatch = np.transpose(colouredPatch, (1, 2, 0))
                stimImage[ i*pchSz:(i+1)*pchSz, j*pchSz:(j+1)*pchSz   ] = thisPatch
                groupImage[i*pchSz:(i+1)*pchSz, j*pchSz:(j+1)*pchSz, :] = colouredPatch

    # Generate both stimulus and group plots
    if plot:
        plt.subplot(2, 1, 1)
        plt.imshow(stimImage, cmap='gray')
        plt.subplot(2, 1, 2)
        plt.imshow(groupImage)
        plt.show()

    # Return the whole images to the program
    return stimImage, groupImage


def segmentStim(groupList, groupImage, stimImage, nSegLayers, targetPos, pchSz, boumaRadius, segRadius, segType):
    
    # Initialize the variables and arrays
    nSegSignals = nSegLayers - 1
    segColours  = createSpacedColors(7)
    signalsMap  = np.zeros(groupImage.shape)
    segLists    = np.ones((nSegLayers,) + groupList.shape, dtype=int)*(-1)
    segLists[0] = groupList

    # Uniform distribution anywhere on the display
    if segType == 'uniform':
        segRows = np.random.RandomState().randint(low=0, high=groupImage.shape[0], size=(nSegSignals,))
        segCols = np.random.RandomState().randint(low=0, high=groupImage.shape[1], size=(nSegSignals,))
   
    # Circle-shaped signals fall on a crown around the target (one in each quarter, like one on each side in Francis 2017)
    if segType == 'around':
        segRads = np.random.RandomState().normal(loc=boumaRadius*pchSz*0.5, scale=boumaRadius*pchSz*0.25, size=(nSegSignals,))
        # segTets = np.random.RandomState().random((nSegSignals,))*2.0*np.pi
        segTets = np.array([np.random.RandomState().random()*2*np.pi/nSegSignals + h*2*np.pi/nSegSignals
            for h in np.random.RandomState().permutation(range(nSegSignals))])
        segRows = ((targetPos[0]+0.25)*pchSz + segRads*np.sin(segTets)).astype(int)
        segCols = ((targetPos[1]+0.25)*pchSz + segRads*np.cos(segTets)).astype(int)

    # Go through every segmentation layer
    segLocs = list(zip(segRows, segCols))
    for h in range(nSegSignals):
        (signalRow, signalCol) = segLocs[h]
        # distToFix  = np.sqrt((signalRow - (targetPos[0]+0.5)*pchSz)**2 + (signalCol - (-0.5*pchSz))**2)
        # thisSegRad = int(distToFix*segRadius/100)
        thisSegRad = segRadius

        # Select where the signal exactly hit the stimulus and draw the signal
        for i in range(signalRow - thisSegRad, signalRow + thisSegRad + 1):
            for j in range(signalCol - thisSegRad, signalCol + thisSegRad + 1):
                if 0 < i < groupImage.shape[0] and 0 < j < groupImage.shape[1]:                   
                    if (i-signalRow)**2 + (j-signalCol)**2 < thisSegRad**2:

                       # Draw the signal and shift activity that was touched by the signal
                        signalsMap[i, j, :] += [0.5 * a for a in segColours[h]]
                        if any(groupImage[i, j, :] != 0):
                            (r, c) = (i//pchSz, j//pchSz)
                            if segLists[0, r, c] != -1:
                                segLists[0  , :, :][groupList == groupList[r, c]] = -1
                                segLists[h+1, :, :][groupList == groupList[r, c]] = groupList[r, c]

    # Draw the interaction window (bouma-sized disk) around target location
    for i in range(int(pchSz * (targetPos[0] - boumaRadius)), int((pchSz + 1) * (targetPos[0] + boumaRadius) + 1)):
        for j in range(int(pchSz * (targetPos[1] - boumaRadius)), int((pchSz + 1) * (targetPos[1] + boumaRadius) + 1)):
            if 0 < i < groupImage.shape[0] and 0 < j < groupImage.shape[1]:
                if (i - ((pchSz+0.5)*targetPos[0]-1))**2+(j-((pchSz+0.5)*targetPos[1]-1))**2 < ((pchSz+0.5)*boumaRadius)**2:
                    signalsMap[i, j, :] += [0.25 * a for a in segColours[nSegSignals]]

    # Return the segmentation layers output to the program
    return signalsMap, segLists