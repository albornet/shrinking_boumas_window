#######################
### Capsule network ###
#######################
import tensorflow as tf
from make_tf_data import make_stim_tfrecords
from parameters import parameters
from capser_input_fn import predict_input_fn
from specs_genetic import *

def computeCapserPerformance(model, stim, plot=False, nTrials=nTrials):

    stim_path = './Capsules/' + parameters.test_data_path
    make_stim_tfrecords(stim_path, stim, max(nTrials, parameters.batch_size))
    if plot:
        import matplotlib.pyplot as plt
        with tf.Session() as sess:
            data_out, _ = predict_input_fn(stim_path)  # parameters.test_data_path)
            img, labels = sess.run([data_out['images'], data_out['targori_label']])
            plt.imshow(img[0, :, :, 0])
            plt.title('Class label = ' + str(labels[0]) + ', IM_SIZE = ' + str(img.shape))
            plt.show()

    capser_out = list(model.predict(lambda: predict_input_fn(stim_path)))
    return [p['targori_accuracy'] for p in capser_out][0]