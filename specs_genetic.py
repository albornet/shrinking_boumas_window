###############
### Imports ###
###############
import numpy as np
import random


##################
### Parameters ###
##################

# Display parameters
xDim     = 19     # number of elements on the x-dimension (col)
yDim     = 15     # number of elements on the y-dimension (row)
targEcc  = 6      # target eccentricity in degrees
slotSize = 0.75   # distance between two elements in degrees
targPosX = int(targEcc/slotSize)-1  # grid col of the target
targPosY = int(yDim/2.0)            # grid row of the target
targPos  = [targPosY, targPosX]     # row is x and col is y

# Genetic algorithm parameters
nSubjects         =  10  # 10 number of participant in the experiment
nSessions         =   4  #  4 number of full GA run per participant
nTrials           =  12  # 12 number of trial per display
nGenerations      =   6  #  6 number of generation in the GA
nStartingDisplays =  20  # 20 number of displays for the first generation
nEvolvedDisplays  =  12  # 12 number of displays for the next generations
nParents          =   4  #  4 number of winner parents used to create the next generation
mutationRate      =   4  #  4 percentage of mutation at each new generation
initialProp       =   0.30*np.ones((nSubjects, nSessions))                               # closer to 1.0 is more verticals
# initialProp      +=   0.01*(np.random.RandomState().random((nSubjects, nSessions))-0.5)  # make some noise on init prop
modelPeople       =   0
if modelPeople:
    initialProp = np.array([
        [0.51, 0.51, 0.51, 0.51],
        [0.22, 0.22, 0.22, 0.22],
        [0.25, 0.25, 0.25, 0.25],
        [0.40, 0.40, 0.40, 0.40],
        [0.24, 0.24, 0.24, 0.24],
        [0.26, 0.26, 0.26, 0.26],
        [0.40, 0.50, 0.50, 0.50],
        [0.21, 0.21, 0.21, 0.21],
        [0.22, 0.30, 0.30, 0.30],
        [0.20, 0.20, 0.20, 0.20]])
    initialProp = initialProp[0:nSubjects]
    nSessions   = initialProp.shape[1]


###################################
### Genetic algorithm functions ###
###################################

def setTheFirstGeneration(nStartingDisplays, xDimension, yDimension, initialProp):

    stimDims = (nStartingDisplays, xDimension, yDimension)
    displays = (np.random.RandomState().choice([-1,1], size=stimDims, p=[1.0-initialProp, initialProp])).astype(int)
    return displays

def xOver(parent1, parent2):

    xDimension, yDimension = parent1.shape
    child1 = np.zeros((xDimension, yDimension))
    child2 = np.zeros((xDimension, yDimension))
    for x in range(0, xDimension):
        for y in range(0, yDimension):
            if random.randint(1, 100) <= 50:  # 70? It seems 50 is used in the evolving the keys paper.
                child1[x][y] = parent1[x][y]
                child2[x][y] = parent2[x][y]
            else:
                child1[x][y] = parent2[x][y]
                child2[x][y] = parent1[x][y]

    return child1, child2

def mutation(display, mutationRate, initialProp):

    xDimension, yDimension = display.shape
    for x in range(0, xDimension):
        for y in range(0, yDimension):
            if random.randint(1, 100) <= mutationRate:
                if random.randint(1, 100) <= initialProp:
                    display[x][y] =  1
                else:
                    display[x][y] = -1

    return display

def evolveTheDisplays(bestDisplays, nParents, displ, nStartingDisplays, mutationRate, initialProp):
    
    dispNum, xDimension, yDimension = displ.shape
    evolvedDisplays = np.zeros((nStartingDisplays, xDimension, yDimension), dtype=int)
    counter         = 0
    
    for parent1 in range(0, nParents):
        for parent2 in range(parent1+1, nParents):

            baby1, baby2 = xOver(displ[bestDisplays[parent1]], displ[bestDisplays[parent2]])
            evolvedDisplays[counter  ] = mutation(baby1, mutationRate, initialProp)
            evolvedDisplays[counter+1] = mutation(baby2, mutationRate, initialProp)
            counter += 2

    return evolvedDisplays


################################
### Random model definitions ###
################################

def computeRandomPerformance():

    nCorrects = 0
    for n in range(nTrials):
        nCorrects += np.random.RandomState().choice([0,1], p=[0.33, 0.67])
    return nCorrects/nTrials