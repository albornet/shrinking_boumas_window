import torch
import matplotlib.pyplot as plt
import numpy as np
import numba
import random
from scipy import ndimage


# Create patch with defined orientation
def create_patch(ori, patch_size):  
    space = 1
    patch = np.zeros((patch_size, patch_size))
    patch[space:-1-(space-1), (patch_size-1)//2] = 1.0
    patch = ndimage.rotate(patch, ori, order=1, reshape=False)
    return patch


# Create patches that are always used
patch_size  = 11
tarL_patch  = create_patch( 10, patch_size)
tarR_patch  = create_patch(-10, patch_size)
hori_patch  = create_patch( 90, patch_size)
vert_patch  = create_patch(  0, patch_size)
flk_patches = [vert_patch, hori_patch]


# Efficient noise adding function
@numba.njit
def add_noise(x, level):
    x = x.reshape(-1)
    for ii in range(len(x)):
        x[ii] += level*random.random()


# Make a dataset to train the network
def make_dataset(device, stim_type, set_type, n_samples, image_size=(3,227,227),\
                 noise_level=0.1, stim_shape=(15,19), normal_targ_pos=(7,7)):

    # Initilaize inputs and labels
    data  = np.zeros((n_samples,)+image_size[1:])
    label = torch.zeros((n_samples,), dtype=torch.long, device=device)

    # Generate the samples
    do_like_caps = False
    no_overlap   = True
    free_target  = False
    if free_target:
        min_tar_pos  = [7-1, 7-1]
        max_tar_pos  = [7+2, 7+2]  # stim_shape
    min_flk_size = [0, 0]
    max_flk_size = stim_shape
    for n in range(n_samples):

        # Training batch (random target position and non-overlapping flanker array)
        if set_type == 'train':
            targ_pos   = [np.random.randint(s, t  ) for s, t in zip(min_tar_pos,  max_tar_pos )] if free_target else normal_targ_pos
            side_size  = [np.random.randint(s, t  ) for s, t in zip(min_flk_size, max_flk_size)]
            flk_origin = [np.random.randint(0, s-t) for s, t in zip(stim_shape,   side_size   )]
            if (not do_like_caps) or no_overlap:
                while (flk_origin[0] <= targ_pos[0] <= flk_origin[0] + side_size[0]\
                   and flk_origin[1] <= targ_pos[1] <= flk_origin[1] + side_size[1]):
                        targ_pos   = [np.random.randint(s, t  ) for s, t in zip(min_tar_pos,  max_tar_pos )] if free_target else normal_targ_pos
                        side_size  = [np.random.randint(s, t  ) for s, t in zip(min_flk_size, max_flk_size)]
                        flk_origin = [np.random.randint(0, s-t) for s, t in zip(stim_shape,   side_size   )]

        # Testing batch (either full GA array or target only)
        else:
            targ_pos   = normal_targ_pos
            flk_origin = [0, 0]
            if set_type == 'test1':
                side_size = stim_shape
            else:
                side_size = [0, 0]

        # Draw the flanker array
        if (not do_like_caps) or (set_type != 'train' or n%2 == 0):
            if side_size[0] > 0 and side_size[1] > 0:
                if stim_type == 'normal' or set_type == 'test1':
                    flk_array   = None
                    for r in range(side_size[0]):
                        flk_choice = np.random.choice([0, 1], side_size[1], p=[0.3, 0.7])
                        flk_line   = np.hstack([flk_patches[c] for c in flk_choice])
                        flk_array  = np.vstack([flk_array, flk_line]) if flk_array is not None else flk_line
                elif stim_type == 'allsame':
                    flk_patch = flk_patches[np.random.choice([0, 1])]  # p=[v_prop, 1-v_prop])
                    flk_array = np.vstack(side_size[0]*[np.hstack(side_size[1]*[flk_patch])])
                first_row = int((image_size[1]-stim_shape[0]*patch_size)/2 + flk_origin[0]*patch_size)
                first_col = int((image_size[2]-stim_shape[1]*patch_size)/2 + flk_origin[1]*patch_size)
                data[n, first_row:first_row+flk_array.shape[0], first_col:first_col+flk_array.shape[1]] = flk_array

        # Draw the target
        if (not do_like_caps) or (set_type != 'train' or n%2 != 0):
            label[n]   = np.random.choice([0, 1])
            targ_patch = [tarL_patch, tarR_patch][label[n]]
            first_row  = int((image_size[1]-stim_shape[0]*patch_size)/2 + targ_pos[0]*patch_size)
            first_col  = int((image_size[2]-stim_shape[1]*patch_size)/2 + targ_pos[1]*patch_size)
            data[n, first_row:first_row+targ_patch.shape[0], first_col:first_col+targ_patch.shape[1]] = targ_patch

    # Add noise and normalize
    if noise_level > 0.0:
        add_noise(data, noise_level)

    # Plot things (or not) and return the batch to the computer    
    data = torch.tensor(data, dtype=torch.float, device=device).unsqueeze(1).expand((n_samples,)+image_size)
    if 0:
        data = (data-data.min())/(data.max()-data.min())
        for n in range(10):
            plt.imshow(data[n].cpu().numpy().transpose((1,2,0)))
            plt.title('Set: %s - Target: %s' % (set_type, ['L', 'R'][label[n]]))
            plt.show()
    return data, label