import torch
import torch.nn as nn
import os
from batch_maker import make_dataset
from models import get_output_size, AlexNet, Classifier

# Model and learning parameters
batch_size =  16
n_batches  =  16
n_samples  =  n_batches*batch_size
n_epochs   =  10000
learn_rate =  1e-6
ns         = [4, 7, 9, 11, 15]
use_gpu    =  True
use_check  =  True
stim_type  = 'allsame'  # 'allsame' or 'normal'

# Create AlexNet and classifiers
my_alexnet  =  AlexNet()
my_alexnet.eval()  # deactivate dropout in alexnet
input_sizes = [get_output_size(my_alexnet, n) for n in ns]
models      = [Classifier(input_size=s) for s in input_sizes]

# Use GPU if available
device = torch.device('cpu')
if use_gpu and torch.cuda.is_available():
    print('GPU power will be used to train the model')
    device     =  torch.device( 'cuda')
    my_alexnet =  my_alexnet.to('cuda')
    models     = [model.     to('cuda') for model in models]
else:
    print('CPU power will be used to train the model')

# Load weights if possible
model_path = './model_checkpoints_%s' % (stim_type)
if not os.path.exists(model_path):
    os.mkdir(model_path)
for i, model in enumerate(models):
    this_model_path = model_path+'/layer_'+str(ns[i])+'_subject.pt'
    if use_check and os.path.exists(this_model_path):
        model.load_state_dict(torch.load(this_model_path))

# Learning devices
crit   =  nn.CrossEntropyLoss()
optims = [torch.optim.Adam(m.parameters(), lr=learn_rate) for m in models]
scheds = [torch.optim.lr_scheduler.CosineAnnealingWarmRestarts(o, T_0=1000, T_mult=2) for o in optims]

# Epoch loops
print('\nTraining...')
for e in range(n_epochs):

    # Generate a training set and feed it to AlexNet
    stims, labels = make_dataset(device=device, stim_type=stim_type, set_type='train', n_samples=n_samples)
    with torch.no_grad():
        trains = my_alexnet(stims, ns)

    # Training loop
    n_train_errs = [[0]*len(ns) for i in range(1)]
    for b_index, b in enumerate(range(0, stims.size(0), batch_size)):

        # Take a batch from AlexNet output
        inputs = [train .narrow(0, b, batch_size) for train in trains]
        target =  labels.narrow(0, b, batch_size)
    
        # Feed the output of AlexNet to the classifiers
        for i, model in enumerate(models):
            model.train()
            outputs = model(inputs[i])
            loss    = crit(outputs, target)
            n_train_errs[0][i] += (outputs.argmax(1) != target).sum()

            # Back-propagation
            model.zero_grad()
            loss.backward()
            optims[i].step()
            scheds[i].step(e + b_index/batch_size)

    # Testing loop
    if e % 10 == 0:
        n_test_errs = [[0]*len(ns) for i in range(2)]
        for index, set_type in enumerate(['test1', 'test2']):

            # Generate a testing set and feed it to AlexNet
            stims, labels = make_dataset(device=device, stim_type=stim_type, set_type=set_type, n_samples=n_samples)
            with torch.no_grad():
                tests = my_alexnet(stims, ns)
                for b in range(0, stims.size(0), batch_size):

                    # Take a batch from AlexNet output
                    inputs = [test  .narrow(0, b, batch_size) for test in tests]
                    target =  labels.narrow(0, b, batch_size)
                    
                    # Feed the output of AlexNet to the classifiers
                    for i, model in enumerate(models):
                        model.eval()
                        outputs = model(inputs[i])
                        n_errs  = (outputs.argmax(1) != target).sum()
                        n_test_errs[index][i] += n_errs

        # Print training and testing errors
        print('\n\nEpoch %i - Learning rate %.2fe-5\n' % (e, 1e5*scheds[0].get_last_lr()[0]))
        for i, n in enumerate(ns):
            train_err = 100*float(n_train_errs[0][i])/n_samples
            test1_err = 100*float(n_test_errs[ 0][i])/n_samples
            test2_err = 100*float(n_test_errs[ 1][i])/n_samples
            print('  Layer %2i - train error flanked: %5.2f %% - test error flanked: %5.2f %% - test error target: %5.2f %%'\
               % (n, train_err, test1_err, test2_err))

            # If the error rate in the GA range, keep the layer weights
            if 30.0 < test1_err < 34.0:
                from shutil import copyfile
                src_model_path = '%s/layer_%i_subject.pt' % (model_path, ns[i])
                src_model_path = '%s/layer_%i_accur67.pt' % (model_path, ns[i])
                try:
                    copyfile(src_model_path, dst_model_path)
                except:
                    pass

    # Print training error at each epoch      
    else:
        print('\nEpoch %i - Learning rate %.2fe-5\n' % (e, 1e5*scheds[0].get_last_lr()[0]))
        for i, n in enumerate(ns):
            print('  Layer %2i - train error flanked: %5.2f %%' % (n, 100*float(n_train_errs[0][i])/n_samples)) 

    # Save the networks every 10 epochs
    if (e+1) % 10 == 0:
        for i, model in enumerate(models):
            this_model_path = '%s/layer_%i_subject.pt' % (model_path, ns[i])
            torch.save(model.state_dict(), this_model_path)
