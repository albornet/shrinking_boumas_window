import torch
import torch.nn as nn
import torch.utils.model_zoo as model_zoo
import functools


# Give the size of a layer of alexnet given input size
def get_output_size(model, n, input_size=(3, 227, 227)):
    
    output_sizes = []
    x = torch.zeros(input_size).unsqueeze_(dim=0)
    for layer in list(model.features):
        x = layer(x)
        output_sizes.append(x.size()[1:])

    x = x.view(x.size(0), functools.reduce(lambda a, b: a*b, x.size()[1:]))
    for layer in list(model.classifier):
        x = layer(x)
        output_sizes.append(x.size()[1:])

    return output_sizes[n]


# AlexNet network
class AlexNet(nn.Module):

    def __init__(self):
        super(AlexNet, self).__init__()
        
        self.features = nn.Sequential(
            nn.Conv2d(3, 64, kernel_size=11, stride=4, padding=2),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=3, stride=2),
            nn.Conv2d(64, 192, kernel_size=5, padding=2),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=3, stride=2),
            nn.Conv2d(192, 384, kernel_size=3, padding=1),
            nn.ReLU(inplace=True),
            nn.Conv2d(384, 256, kernel_size=3, padding=1),
            nn.ReLU(inplace=True),
            nn.Conv2d(256, 256, kernel_size=3, padding=1),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=3, stride=2))

        self.classifier = nn.Sequential(
            nn.Dropout(),
            nn.Linear(256*6*6, 4096),
            nn.ReLU(inplace=True),
            nn.Dropout(),
            nn.Linear(4096, 4096),
            nn.ReLU(inplace=True),
            nn.Linear(4096, 1000))

        self.load_state_dict(model_zoo.load_url('https://download.pytorch.org/models/alexnet-owt-4df8aa71.pth'))
        for p in self.parameters():
            p.requires_grad = False

    def forward(self, x, ns):

        ys = []       
        for n, layer in enumerate(self.features):
            x = layer(x)
            if n in ns:
                ys.append(x)
        
        x = x.view(x.size(0), functools.reduce(lambda a, b: a*b, x.size()[1:]))
        for n, layer in enumerate(self.classifier):
            x = layer(x)
            if n+len(self.features) in ns:
                ys.append(x)

        return ys


# Classifier with one hidden layer
class Classifier(nn.Module):

    def __init__(self, input_size, n_hidden=512):      
        super(Classifier, self).__init__()

        input_len = functools.reduce(lambda a, b: a*b, input_size)
        self.input_size       = input_size
        self.input_len        = input_len
        self.late_classifiers = nn.Sequential(
            nn.BatchNorm1d(input_len),
            nn.Linear(input_len, n_hidden),
            nn.ELU(inplace=True),
            nn.Linear(n_hidden, 2),
            nn.Softmax(dim=0))

    def forward(self, x):
        x = x.view(x.size(0), -1)
        return self.late_classifiers(x)