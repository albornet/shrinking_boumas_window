##################
### Your model ###
##################
from specs_genetic import *
import numpy as np
# import yourNeededPackage

def computeYourModelPerformance(stim, nTrials=nTrials):

    # This function must return model accuracy (proportion correct)
    # Note that stim only encodes flanker orientation (1=vert, -1=hori)
    # If you want the target orientation ('L' or 'R') to be encoded in
    #  stim, you must set it yourself (stim[targPos[0], targPos[1]] = ...)
    nCorrects = 0
    for n in range(nTrials):
        targOri = np.random.choice(
            a=['L', 'R'],
            p=[0.5, 0.5])
        yourModelOutput = yourModelFunction(stim, targOri)
        correct = yourPerfFunction(yourModelOutput, targOri)
        nCorrects += correct
    return nCorrects / nTrials  # between 0.0 and 1.0

def yourModelFunction(stim, targOri):

    # Code of your model
    # The model performance must be close to 0.67 in the first GA generation
    # yourModelParam1 = ???
    # yourModelParam2 = ???
    # etc.
    # yourModelOutput = ???
    yourModelOutput = np.random.choice(
        a=['L', 'R', targOri],
        p=[0.33, 0.33, 0.34])  # dumb example to have 0.67 of accuracy
    return yourModelOutput

def yourPerfFunction(yourModelOutput, targOri):

    # Code to transform your model output in a correct answers
    # isCorrect = ???
    isCorrect = int(yourModelOutput == targOri)  # dumb example
    return isCorrect
