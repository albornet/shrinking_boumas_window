% Example 1: Synthesis of a "text" texture image, using
% Portilla-Simoncelli texture analysis/synthesis code, based on
% alternate projections onto statistical constraints in a complex
% overcomplete wavelet representation.
%
% See Readme.txt, and headers of textureAnalysis.m and
% textureSynthesis.m for more details.
%
% Javier Portilla (javier@decsai.ugr.es).  March, 2001
%
% Modified by Adrien Doerig to run crowding samples May, 2017
%
% see runAll to see how function is used. run from runAll
    
function [] = texturizeCrowding(stim, offset, iteration)

    % you can choose to use a low-pass filtered noisy version of the image
    % as the seed for the texturizing
    
    useLowPassNoisySeed = 1;
    if useLowPassNoisySeed
        filterOrder = 100;
        filterCutoff = 0.15;
        noiseGain = 0.2;
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % directory management
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    [progPath,~,~] = fileparts(which(mfilename)); % get program directory
    cd(progPath); % go there just in case we're not already

    % if there's no results directory, it's a new stimulus
    if exist([progPath filesep '\results\'], 'dir') == 0
        mkdir('results');
    end
    if exist([progPath filesep '\results\' filesep stim], 'dir') == 0
        cd([progPath, '\results\'])
        mkdir(stim)
    end
    if exist([progPath filesep '\results\' filesep stim filesep offset],'dir') == 0
        cd([progPath, '\results\' filesep stim])
        mkdir(offset)
    end

    % define paths
    resPath = [progPath, '\results\' filesep stim filesep offset]; % path to data folder
    stimPath = [progPath, '\stimuli\' offset];

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % setup stimuli
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    cd(stimPath);
    files = dir([stimPath, '\', stim, '*.mat']);
    names = {files.name};
    
    % this will loop over all stimuli
    for i = 1:length(names)
        cd(stimPath)
        names{i} = names{i}(1:end-4);
        im0 = loadSingleVariableMATFile(names{i});
        im0 = makeSquareImg(im0,200);
        im0 = imresize(im0, [256 256]);
   
        cd(progPath);

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % texturize
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        Nsc = 4; % Number of scales
        Nor = 4; % Number of orientations
        Na = 9;  % Spatial neighborhood is Na x Na coefficients
             % It must be an odd number!

        params = textureAnalysis(im0, Nsc, Nor, Na);

        Niter = 25;	% Number of iterations of synthesis loop
        Nsx = 256;	% Size of synthetic image is Nsy x Nsx
        Nsy = 256;	% WARNING: Both dimensions must be multiple of 2^(Nsc+2)

        ended = 0;
        it = 0;
        nextStim = 0;
        while ended == 0
            it = it + 1;
            if useLowPassNoisySeed
                seed = makeLowPassNoisyImg(im0, filterOrder, filterCutoff, noiseGain);
                [res, ~, ~, ended] = textureSynthesis(params, seed, Niter);
            else
                [res, ~, ~, ended] = textureSynthesis(params, [Nsy Nsx], Niter);
            end
            if it == 1000 % if you can't get the texture so be it: next stimulus
                ended = 1;
                nextStim = 1; 
            end
        end
        
        if nextStim == 1 % if you can't get the texture so be it: next stimulus
            continue
        end
        
        close all
        cd(resPath)
        if iteration == 1 % only plot input on first iteration
            figure(1)
            showIm(im0, 'auto', 1, 'Original texture');
            saveas(gcf,[names{i}, offset, 'InputImage', '.jpg']);
        end
        figure(2)
        showIm(res, 'auto', 1, 'Synthesized texture');
        saveas(gcf,[names{i}, offset, num2str(iteration), '.jpg']);
    end
end
