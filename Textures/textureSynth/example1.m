% Example 1: Synthesis of a "text" texture image, using
% Portilla-Simoncelli texture analysis/synthesis code, based on
% alternate projections onto statistical constraints in a complex
% overcomplete wavelet representation.
%
% See Readme.txt, and headers of textureAnalysis.m and
% textureSynthesis.m for more details.
%
% Javier Portilla (javier@decsai.ugr.es).  March, 2001

addpath(['..', filesep, ',matlabPyrTools'])
ended = 0;
if ended == 0 % just a trick to keep the program running while it bugs (which may happen depending on the picture (it happens a lot for vernier, not at all for larger stimuli)), until it works
    close all
    
%     im0 = pgmRead('rightVernier.pgm');	% im0 is a double float matrix!
% im0 = createInputImg('rightCircles_4')
    im0 = double(rgb2gray(imread('fig6aStim(1).png')))
    
    Nsc = 4; % Number of scales
    Nor = 4; % Number of orientations
    Na = 9;  % Spatial neighborhood is Na x Na coefficients
         % It must be an odd number!

    params = textureAnalysis(im0, Nsc, Nor, Na);

    Niter = 25;	% Number of iterations of synthesis loop
    Nsx = 256;	% Size of synthetic image is Nsy x Nsx
    Nsy = 256;	% WARNING: Both dimensions must be multiple of 2^(Nsc+2)

    res = textureSynthesis(params, [Nsy Nsx], Niter);

    close all
    figure(1)
    showIm(im0, 'auto', 1, 'Original texture');
    figure(2)
    showIm(res, 'auto', 1, 'Synthesized texture');

    ended = 1;
end
% Can you read the NEW text? ;-)
