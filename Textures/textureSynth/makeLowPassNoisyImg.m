function lowPassNoisyImg = makeLowPassNoisyImg(im0, order, cutoff, noiseGain)
% imagesc(im0)

h = fir1(order,cutoff,'low');
h = h'*h;
imLowPass = filter2(h,im0,'same');
lowPassNoisyImg = imLowPass + noiseGain*randn(size(imLowPass));

% imagesc(im0)