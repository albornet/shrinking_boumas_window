function im0 = createInputImg(img)
% CREATEINPUTIMG from .mat file: takes a .mat file, makes it square.
% 
% ex. createInputImg('vernierL')

im0 = loadSingleVariableMATFile([img, '.mat']);
im0 = makeSquareImg(im0, 250);  % this acts like a zoom
im0 = imresize(im0, [256 256]);
figure()
imagesc(im0)
colormap gray
truesize
