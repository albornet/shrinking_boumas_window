% NOTE: you can choose to use a low-pass filtered noisy version of the image
% as the seed for the texturizing: see in texturizeCrowding

clear
stims = ...%{'vernier', 
    {'circles', 'gestalts', 'hexagons', 'irreg1', 'irreg2', ...
'malaniaEqual', 'malaniaLong', 'malaniaShort', 'octagons' ...
'pattern2', 'patternIrregular', 'patternStars', 'squares', 'stars'};
offset = {'left','right'};

texturize = 1;
compCrowding = 0;
plotCrowding = 0;

[progPath,~,~] = fileparts(which(mfilename)); % get program directory
cd(progPath); % go there just in case we're not already
addpath(['..', filesep, 'matlabPyrTools']) % if this doesn't work, add the folders manually, i don't know what the problem is-
addpath(['..', filesep, 'textureSynth'])
addpath('stimuli')
for iteration = 1:5 % number of samples of each texture

    if texturize
        for run = 1:length(stims)
            disp(['texturizing ' stims{run}])
            for ofst = 1:2
                texturizeCrowding(stims{run},offset{ofst},iteration);
            end
        end
    end

    if compCrowding
        crowding = cell(2,length(stims));
        for run = 1:length(stims)
            disp(['computing crowding for ' stims{run}])
            crowding{1,run} = stims{run};
            crowding{2,run} = computeCrowding(stims{run},iteration);
        end
        cd(['results\patchSide', num2str(iteration)])
        save('crowding','crowding')
        cd(progPath)
    end

    if plotCrowding
        crowdingPlot(crowding, iteration);
    end
end