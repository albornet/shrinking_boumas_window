function squareImg = makeSquareImg(img, side)
% MAKESQUAREIMG takes an image and makes it a square, centered in the middle, 
% of size side*side.

if any(size(img)-side<=0)
    warning('image smaller than size of square. Padding in black...')
    paddedImg = zeros(side+2, side+2); % the +2 is to avoid number rounding problems
    if size(img,1)-side<=0 && size(img,2)-side<=0 % need padding in both dimensions
        paddedImg(round((side-size(img,1))/2):round((side-size(img,1))/2)+size(img,1),...
                  round((side-size(img,2))/2):round((side-size(img,2))/2)+size(img,2)) = img;
        img = paddedImg;
    elseif size(img,2)-side>0 % need padding to create extra lines
        img = [zeros(round((side-size(img,1))/2)+1,size(img,2)); img; zeros(round((side-size(img,1))/2)+1,size(img,2))];
    else % need padding to create extra columns
        img = [zeros(round((side-size(img,1))/2),size(img,2))+1, img, zeros(round((side-size(img,1))/2)+1,size(img,2))];
    end
end

cropSize = [floor((size(img,1)-side)/2), floor((size(img,2)-side)/2)];

img = img(cropSize(1):cropSize(1)+side,:);
img = img(:,cropSize(2):cropSize(2)+side);

squareImg = img;