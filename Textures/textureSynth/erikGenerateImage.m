function [res, stimImage, im4texture] = erikGenerateImage(stim, targEcc, slotSize, targOri, doBoumImage, saveTextures)

    % BAR_SIZE MUST BE AN EVEN NUMBER
    w = warning ('off','all');
    % Takes a configuration and outputs the associated image
    barSize   =  16;  % in pixels
    barWidth  =   2;  % in pixels
    gapSize   =   3;  % in pixels
    stimShp   = size(stim);
    targPos   = [ceil(stimShp(1)/2), round(targEcc/slotSize)];  % in slots
    pixPerDeg = (2*gapSize + barSize)/slotSize;  % number of pixels per degree
    boumaRx   = round(pixPerDeg*targEcc/2);      % in pixels
    boumaRy   = round(pixPerDeg*targEcc/4);      % in pixels

    stim(targPos(1), targPos(2)) = 4;
    stimImage = createImages(stim, barSize, barWidth, gapSize, targOri);
    imSize    = size(stimImage);

    Nsc  = 4;  % Number of scales
    Nor  = 4;  % Number of orientations
    Na   = 9;  % Spatial neighborhood is Na x Na coefficient (must be odd!)
    NVdB = 2^(Nsc+2);

    firstRow = (targPos(1)-0.5)*slotSize*pixPerDeg - boumaRy;  % in pixels
    lastRow  = (targPos(1)-0.5)*slotSize*pixPerDeg + boumaRy;  % in pixels
    firstCol = (targPos(2)-0.5)*slotSize*pixPerDeg - boumaRx;  % in pixels
    lastCol  = (targPos(2)-0.5)*slotSize*pixPerDeg + boumaRx;  % in pixels

    remainRow = NVdB - mod(lastRow-firstRow, NVdB);
    remainCol = NVdB - mod(lastCol-firstCol, NVdB);

    firstRow = firstRow - floor(remainRow/2);
    lastRow  = lastRow  + ceil( remainRow/2);
    firstCol = firstCol - floor(remainCol/2);
    lastCol  = lastCol  + ceil( remainCol/2);
    boumImage = stimImage(firstRow:lastRow, firstCol:lastCol);

    im4texture = stimImage + 0.1*randn(size(stimImage,1),size(stimImage,2));
    if doBoumImage
        im4texture = boumImage + 0.1*randn(size(boumImage,1),size(boumImage,2));
    end

    res = [];
    if saveTextures

        size4texture = size(im4texture);
        extraPixelsX = mod(size4texture(2), 2^(Nsc+2));
        extraPixelsY = mod(size4texture(1), 2^(Nsc+2));

        croppedImage = im4texture(1+ceil(extraPixelsY/2):end-floor(extraPixelsY/2), 1+ceil(extraPixelsX/2):end-floor(extraPixelsX/2));
        croppedImSize = size(croppedImage);
        params = textureAnalysis(croppedImage, Nsc, Nor, Na);

        Niter = 25;	% Number of iterations of synthesis loop
        Nsx = croppedImSize(2);	% Size of synthetic image is Nsy x Nsx
        Nsy = croppedImSize(1);	% WARNING: Both dimensions must be multiple of 2^(Nsc+2)

        ended = 0;
        it = 0;
        while ended == 0
            [res, ~, ~, ended] = textureSynthesis(params, [Nsy Nsx], Niter);
            if it == 20 % if you can't get the texture so be it: forget about the whole thing
                ended = 1;
            end
        end
    end

end

% Return the patches to the program (1=vertical, 2=horizontal, 3=void, 4=target)
function stimPatches = createPatches(barSz, barWi, gapSz, targOri)

    % Initialize vertical, horizontal, empty and target patches
    pchSz        = 2*gapSz + barSz;
    unos         = [zeros(gapSz,1); ones(barSz,1); zeros(gapSz,1)];
    zero         = zeros(pchSz,1);
    voidPatch    = zeros(pchSz);
    vertPatch    = [repmat(zero, [1,floor(pchSz/2)-ceil(barWi/2)]),repmat(unos,  [1,barWi]), repmat(zero, [1,floor(pchSz/2)-floor(barWi/2)])];               
    horiPatch    = [repmat(zero',[floor(pchSz/2)-ceil(barWi/2),1]);repmat(unos', [barWi,1]);repmat(zero',[floor(pchSz/2)-floor(barWi/2),1])];

    targPatch = voidPatch;
    rotMat = [cosd(targOri), -sind(targOri); sind(targOri), cosd(targOri)];
    for i = -pchSz/2:pchSz/2-1
        for j = -pchSz/2:pchSz/2-1
            rotCoord = rotMat*[i,j]' + pchSz/2;
            if all(rotCoord >= 1) && all(rotCoord <= pchSz)
                targPatch(i + pchSz/2 + 1,j + pchSz/2 + 1) = vertPatch(ceil(rotCoord(1)),ceil(rotCoord(2)));
            end
        end
    end

    stimPatches = {vertPatch, horiPatch, voidPatch, targPatch};

end

% Create an image corresponding to the stimulus
function stimImage = createImages(stim, barSz, barWi, gapSz, targOri)

    % Initialize all patches and the final images to plot (group and stimulus)
    stimPatches = createPatches(barSz, barWi, gapSz, targOri);
    pchSz       = 2*gapSz + barSz;
    stimDims    = size(stim);
    stimImage   = zeros(stimDims(1)*pchSz, stimDims(2)*pchSz);

    % Go through all the slots to plot the patches
    for i = 1:stimDims(1)
        for j = 1:stimDims(2)
            thisPatch = stimPatches{stim(i,j)};
            stimImage((i-1)*pchSz+1:i*pchSz, (j-1)*pchSz+1:j*pchSz) = thisPatch;
        end
    end

end