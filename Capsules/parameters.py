# -*- coding: utf-8 -*-
"""
My capsnet: all parameters
@author: Adrien Doerig based on code by Lynn Schmittwilken, who based her code on code by Adrien Doerig.
"""

import tensorflow as tf
flags = tf.app.flags

stim_shape    = [9, 9]  # [15, 19]  # number of elements in display
targ_pos      = [4, 4]  # [7, 7]
patch_size    = 9
im_size       = [i*(patch_size+1) for i in stim_shape]
n_classes     = 3  # target / group of horizontal bars / group of vertical bars
targ_ori_ampl = 12.0

###########################
#          Paths          #
###########################
# In general:
data_path = './data'
net_type = 'CAPSNET'  # 'CAPSNET', 'CNN', 'LATERAL_RNN', 'TOPDOWN_RNN'
winners = {'CAPSNET': [0], 'CNN': [0], 'LATERAL_RNN': [0], 'TOPDOWN_RNN': [0]}
MODEL_NAME = '_logs_' + net_type
flags.DEFINE_string('net_type', net_type, 'choose CNN, LNN or TNN')
flags.DEFINE_string('model_name', MODEL_NAME, 'name of the model used in the GA')
flags.DEFINE_string('data_path', data_path, 'path where all data files are located')
flags.DEFINE_string('logdir', data_path + '/' + MODEL_NAME + '/', 'save the model results here')
flags.DEFINE_list('winners', winners[net_type], 'network run index for the GA')

# Stimuli:
flags.DEFINE_string('train_data_path', data_path+'/train.tfrecords', 'path for tfrecords with training set')
flags.DEFINE_string('eval_data_path', data_path+'/eval.tfrecords', 'path for tfrecords with validation set')
flags.DEFINE_string('test_data_path', data_path+'/test.tfrecords', 'path for tfrecords with test set')
flags.DEFINE_string('logdir_reconstruction', data_path + '/' + MODEL_NAME + '_rec/', 'save results with reconstructed weights here')


#####################
#    Noise stuff    #
#####################
flags.DEFINE_integer('random_seed', None, 'if not None, set seed for weights initialization')
flags.DEFINE_float('train_noise', 0.1, 'amount of added random Gaussian noise')
flags.DEFINE_float('eval_noise', 0.05, 'amount of added random Gaussian noise')


###########################
#   Stimulus parameters   #
###########################
# IMPORTANT NOTES:
    # 1. If u change any stimulus parameter, keep in mind that u need to create a new training set.
flags.DEFINE_integer('n_train_samples', 100000, 'number of samples in the training set')
flags.DEFINE_integer('n_eval_samples', 480, 'number of samples in the test set')
flags.DEFINE_list('im_size', im_size, 'image size of datasets')
flags.DEFINE_integer('im_depth', 1, 'number of channels in the datasets')
flags.DEFINE_list('stim_shape', stim_shape, 'size of display (there will be stim_shape[0]*stim_shape[1] elements in the display)')
flags.DEFINE_integer('min_size', 1, 'min number of elements in a side of the square of elements')
flags.DEFINE_integer('max_size', 7, 'max number of elements in a side of the square of elements')
flags.DEFINE_list('targ_pos', targ_pos, 'where to place the target during testing')
flags.DEFINE_integer('patch_size', patch_size, 'size of one bar')
flags.DEFINE_integer('n_classes', n_classes, 'image size of datasets')
flags.DEFINE_float('targ_ori_ampl', targ_ori_ampl, 'amplitude of target orientation (in degrees)')
flags.DEFINE_list('skip_elements', [0.0, 0.0], 'randomly skip a random fraction of elements groups of bars in this range, for each stimulus (not applied to targets')
flags.DEFINE_boolean('small_dataset', False, 'all images are 2x smaller')  # only works for the training set at the moment
flags.DEFINE_boolean('targ_and_flanks', False, 'both (non-overlapping) target and flankers during training')  # only works for the training set at the moment
flags.DEFINE_boolean('separate_images', False, 'get one image for the target, and another for the flanks')  # only works for the training set at the moment
flags.DEFINE_boolean('jitter_only', True, 'each image jitters around the target position instead of being at a random position')


###########################
#   Network parameters    #
###########################
# Conv and primary caps:
caps1_nmaps = 4
caps1_ndims = 1

# Case of 3 conv layers:
kernel1 = 3
kernel2 = 3
kernel3 = 4
stride1 = 1
stride2 = 2
stride3 = 2

# For some reason (rounding/padding?), the following calculation is not always 100% precise, so u might have to add +1:
dim1 = int((((((im_size[0] - kernel1+1) / stride1) - kernel2+1) / stride2) - kernel3+1) / stride3) + 0
dim2 = int((((((im_size[1] - kernel1+1) / stride1) - kernel2+1) / stride2) - kernel3+1) / stride3) + 0

conv1_params = {'filters': caps1_nmaps*caps1_ndims, 'kernel_size': kernel1, 'strides': stride1, 'padding': 'valid'}
conv2_params = {'filters': caps1_nmaps*caps1_ndims, 'kernel_size': kernel2, 'strides': stride2, 'padding': 'valid'}
conv3_params = {'filters': caps1_nmaps*caps1_ndims, 'kernel_size': kernel3, 'strides': stride3, 'padding': 'valid'}
flags.DEFINE_list('conv_params', [conv1_params, conv2_params, conv3_params], 'list with the conv parameters')


flags.DEFINE_integer('caps1_nmaps', caps1_nmaps, 'primary caps, number of feature maps')
flags.DEFINE_integer('caps1_ncaps', caps1_nmaps * dim1 * dim2, 'primary caps, number of caps')
flags.DEFINE_integer('caps1_ndims', caps1_ndims, 'primary caps, number of dims')


# Output caps:
flags.DEFINE_integer('caps2_ncaps', n_classes, 'second caps layer, number of caps')
flags.DEFINE_integer('caps2_ndims', 8, 'second caps layer, number of dims')


# Decoder reconstruction:
flags.DEFINE_boolean('reconstruct', True, 'reconstruct input based on capsules output')
flags.DEFINE_string('rec_decoder_type', 'fc', 'use fc or conv layers for decoding (only with 3 conv layers)')
flags.DEFINE_integer('n_hidden_reconstruction_1', 512, 'size of hidden layer 1 in decoder')
flags.DEFINE_integer('n_hidden_reconstruction_2', 1024, 'size of hidden layer 2 in decoder')
flags.DEFINE_integer('n_output', im_size[0]*im_size[1], 'output size of the decoder')


#########################
#    Hyperparameters    #
#########################
# For training
flags.DEFINE_integer('batch_size', 48, 'batch size')                                       # was 48
flags.DEFINE_float('learning_rate', 0.0002, 'chosen learning rate for training')           # was 0.0001
flags.DEFINE_float('learning_rate_decay_steps', 300000, 'decay for cosine decay restart')  # was 250 (small test)

flags.DEFINE_integer('n_epochs', None, 'number of epochs, if None allow for indifinite readings')
flags.DEFINE_integer('n_steps', 200000, 'number of steps')
flags.DEFINE_integer('n_rounds', 1, 'number of evaluations; full training steps is equal to n_steps times this number')
flags.DEFINE_integer('n_iterations', 10, 'number of trained networks')

flags.DEFINE_integer('buffer_size', 1024, 'buffer size')
flags.DEFINE_integer('eval_steps', 10, 'frequency for eval spec; you need at least eval_steps*batch_size stimuli in the validation set')
flags.DEFINE_integer('eval_throttle_secs', 0, 'minimal seconds between evaluation passes')
flags.DEFINE_integer('iter_routing', 3, 'number of iterations in routing algorithm')
flags.DEFINE_float('init_sigma', 0.01, 'stddev for W initializer')


###########################
#         Losses          #
###########################
flags.DEFINE_boolean('decode_sidesize', True, 'decode the side_size')
sidesize_loss = 'xentropy'
flags.DEFINE_string('sidesize_loss', sidesize_loss, 'either xentropy or squared_diff')
flags.DEFINE_boolean('decode_location', True, 'decode the shapes locations and use location loss')
location_loss = 'xentropy'
flags.DEFINE_string('location_loss', location_loss, 'either xentropy or squared_diff')
flags.DEFINE_boolean('decode_n_elements', False, 'decode number of bars in stimulus')


# Control magnitude of losses
flags.DEFINE_float('alpha_reconstruction', 0.0005, 'alpha for reconstruction')
flags.DEFINE_float('alpha_targori', 5.0, 'alpha for vernieroffset loss')  # 1.0
flags.DEFINE_float('alpha_margin', 0.5, 'alpha for margin loss')
flags.DEFINE_float('alpha_sidesize', 0.4, 'alpha for nshapes loss')
flags.DEFINE_float('alpha_n_elements', 0.2, 'alpha for nshapes loss')


if location_loss=='xentropy':
    flags.DEFINE_float('alpha_xpos_loss', 0.1, 'alpha for loss of x coordinate of shape')
    flags.DEFINE_float('alpha_ypos_loss', 0.1, 'alpha for loss of y coordinate of shape')
elif location_loss=='squared_diff':
    flags.DEFINE_float('alpha_xpos_loss', 0.000004, 'alpha for loss of x coordinate of shape')
    flags.DEFINE_float('alpha_ypos_loss', 0.00005, 'alpha for loss of y coordinate of shape')


# Margin loss extras
flags.DEFINE_float('m_plus', 0.9, 'the parameter of m plus')
flags.DEFINE_float('m_minus', 0.1, 'the parameter of m minus')
flags.DEFINE_float('lambda_val', 0.5, 'down weight of the loss for absent digit classes')


###########################
#     Regularization      #
###########################

flags.DEFINE_boolean('dropout', True, 'use dropout after conv layers 1&2')
flags.DEFINE_boolean('batch_norm_conv', False, 'use batch normalization between every conv layer')
flags.DEFINE_boolean('batch_norm_reconstruction', True, 'use batch normalization for the reconstruction decoder layers')
flags.DEFINE_boolean('batch_norm_orientation', False, 'use batch normalization for the orientation loss layer')
flags.DEFINE_boolean('batch_norm_sidesize', False, 'use batch normalization for the sidesize loss layer')
flags.DEFINE_boolean('batch_norm_n_elements', False, 'use batch normalization for the n_elements loss layer')
flags.DEFINE_boolean('batch_norm_location', False, 'use batch normalization for the location loss layer')

parameters = tf.app.flags.FLAGS