# -*- coding: utf-8 -*-
"""
Functions needed to build the capsnet
Including:
    squash, safe_norm, routing_by_agreement,
    conv_layers, primary_caps_layer, secondary_caps_layer,
    predict_shapelabels, create_masked_decoder_input, compute_margin_loss,
    compute_accuracy, compute_reconstruction, compute_reconstruction_loss,
    compute_orientation_offset_loss, compute_sidesize_loss, compute_location_loss

@author: Adrien Doerig based on code by Lynn Schmittwilken, who based her code on code by Adrien Doerig.
"""

import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import os.path
from parameters import parameters

################################
#    Small helper function:    #
################################
def save_params(save_path, parameters):
    txt_file = save_path + '/parameters.txt'
    if os.path.exists(txt_file):
        raise SystemExit('\nPROBLEM: %s already exists!' % txt_file)
    else:
        with open(save_path + '/parameters.txt', 'w') as f_txt:
            f_py = open('./parameters.py')
            variables = f_py.read()
            f_txt.write(variables)
            print('Parameter values saved.')


################################
#      Squash function:        #
################################
def squash(s, axis=-1, epsilon=1e-7, name=None):
    '''Squashing function as described in Sabour et al. (2018) but calculating
    the norm in a safe way (e.g. see Aurelion Geron youtube-video)'''
    with tf.name_scope(name, default_name='squash'):
        squared_norm = tf.reduce_sum(tf.square(s), axis=axis, keepdims=True)
        safe_norm = tf.sqrt(squared_norm + epsilon)
        squash_factor = squared_norm / (1. + squared_norm)
        unit_vector = s / safe_norm
        s_squashed = squash_factor * unit_vector
        return s_squashed


##############################
#        Safe norm           #
##############################
def safe_norm(s, axis=-1, epsilon=1e-7, keepdims=False, name=None):
    '''Safe calculation of the norm for estimated class probabilities'''
    with tf.name_scope(name, default_name='safe_norm'):
        squared_norm = tf.reduce_sum(tf.square(s), axis=axis, keepdims=keepdims)
        s_norm = tf.sqrt(squared_norm + epsilon)
        return s_norm


################################
#     Routing by agreement:    #
################################
def routing_by_agreement(caps2_predicted, batch_size_tensor, parameters):
    # How often we do the routing:
    def routing_condition(raw_weights, caps2_output, counter):
        output = tf.less(counter, parameters.iter_routing)
        return output
    
    # What the routing is:
    def routing_body(raw_weights, caps2_output, counter):
        routing_weights = tf.nn.softmax(raw_weights, axis=2, name='routing_weights')
        weighted_predictions = tf.multiply(routing_weights, caps2_predicted, name='weighted_predictions')
        weighted_sum = tf.reduce_sum(weighted_predictions, axis=1, keepdims=True, name='weighted_sum')
        caps2_output = squash(weighted_sum, axis=-2, name='caps2_output')
        caps2_output_tiled = tf.tile(caps2_output, [1, parameters.caps1_ncaps, 1, 1, 1], name='caps2_output_tiled')
        agreement = tf.matmul(caps2_predicted, caps2_output_tiled, transpose_a=True, name='agreement')
        raw_weights = tf.add(raw_weights, agreement, name='raw_weights')
        return raw_weights, caps2_output, tf.add(counter, 1)
    
    # Execution of routing via while-loop:
    with tf.name_scope('Routing_by_agreement'):
        # Initialize weights and caps2-output-array
        raw_weights = tf.zeros([batch_size_tensor, parameters.caps1_ncaps, parameters.caps2_ncaps, 1, 1],
                               dtype=tf.float32, name='raw_weights')
        caps2_output = tf.zeros([batch_size_tensor, 1, parameters.caps2_ncaps, parameters.caps2_ndims, 1],
                                dtype=tf.float32, name='caps2_output_init')
        # Counter for number of routing iterations:
        counter = tf.constant(1)
        raw_weights, caps2_output, counter = tf.while_loop(routing_condition, routing_body,
                                                           [raw_weights, caps2_output, counter])
        return caps2_output


###################################
#     Create capser network:      #
###################################
def conv_layers(X, parameters, phase=True):
    with tf.name_scope('1_convolutional_layers'):
        # Conv1:
        if parameters.batch_norm_conv:
            conv1 = tf.layers.conv2d(X, name='conv1', use_bias=False, activation=None, **parameters.conv_params[0])
            conv1 = tf.layers.batch_normalization(conv1, training=phase, name='conv1_bn')
        else:
            conv1 = tf.layers.conv2d(X, name='conv1', activation=None, **parameters.conv_params[0])
        conv1 = tf.nn.elu(conv1)
        if parameters.dropout:
            conv1 = tf.layers.dropout(conv1, rate=.3, training=phase, name='dropout1')

        # Conv2:
        if parameters.batch_norm_conv:
            conv2 = tf.layers.conv2d(conv1, name='conv2', use_bias=False, activation=None, **parameters.conv_params[1])
            conv2 = tf.layers.batch_normalization(conv2, training=phase, name='conv2_bn')
        else:
            conv2 = tf.layers.conv2d(conv1, name='conv2', activation=None, **parameters.conv_params[1])
        conv2 = tf.nn.elu(conv2)
        if parameters.dropout:
            conv2 = tf.layers.dropout(conv2, rate=.3, training=phase, name='dropout2')

        # Conv3:
        conv3 = tf.layers.conv2d(conv2, name='conv3', activation=None, **parameters.conv_params[2])
        conv_output = tf.nn.elu(conv3)
        conv_output_sizes = [conv1.get_shape().as_list(), conv2.get_shape().as_list(), conv3.get_shape().as_list()]
        
        tf.summary.histogram('conv1_output', conv1)
        tf.summary.histogram('conv2_output', conv2)
        tf.summary.histogram('conv3_output', conv3)
        return conv_output, conv_output_sizes


def primary_caps_layer(conv_output, parameters):
    with tf.name_scope('2_primary_capsules'):
        caps1_reshaped = tf.reshape(conv_output, [-1, parameters.caps1_ncaps, parameters.caps1_ndims], name='caps1_reshaped')
        caps1_output = squash(caps1_reshaped, name='caps1_output')
        caps1_output_norm = safe_norm(caps1_output, axis=-1, keepdims=False, name='caps1_output_norm')
        tf.summary.histogram('caps1_output_norm', caps1_output_norm)
        return caps1_output


def secondary_caps_layer(caps1_output, batch_size, parameters, W_init=None):
    with tf.name_scope('3_secondary_caps_layer'):
        # Initialize and repeat weights for further calculations:
        if W_init==None:
            W_init = lambda: tf.random_normal(
                shape=(1, parameters.caps1_ncaps, parameters.caps2_ncaps, parameters.caps2_ndims, parameters.caps1_ndims),
                stddev=parameters.init_sigma, dtype=tf.float32, seed=parameters.random_seed, name='W_init')

        W = tf.Variable(W_init, dtype=tf.float32, name='W')
        W_tiled = tf.tile(W, [batch_size, 1, 1, 1, 1], name='W_tiled')

        # Create second array by repeating the output of the 1st layer 10 times:
        caps1_output_expanded = tf.expand_dims(caps1_output, -1, name='caps1_output_expanded')
        caps1_output_tile = tf.expand_dims(caps1_output_expanded, 2, name='caps1_output_tile')
        caps1_output_tiled = tf.tile(caps1_output_tile, [1, 1, parameters.caps2_ncaps, 1, 1], name='caps1_output_tiled')
    
        # Now we multiply these matrices (matrix multiplication):
        caps2_predicted = tf.matmul(W_tiled, caps1_output_tiled, name='caps2_predicted')
        
        # Routing by agreement:
        caps2_output = routing_by_agreement(caps2_predicted, batch_size, parameters)
        
        # Compute the norm of the output for each output caps and each instance:
        caps2_output_norm = safe_norm(caps2_output, axis=-2, keepdims=True, name='caps2_output_norm')
        tf.summary.histogram('caps2_output_norm', caps2_output_norm[0, :, :, :])
        return caps2_output, caps2_output_norm


################################
#         Margin loss:         #
################################

def predict_type_label(caps2_output_norm, print_shapes=False):
    with tf.name_scope('net_prediction'):
        if parameters.targ_and_flanks:
            labels_pred_proba, labels_pred = tf.nn.top_k(caps2_output_norm[:, 0, :, 0, 0], 2, name='y_proba')
            labels_pred = tf.cast(labels_pred, tf.int64)
            labels_pred_sorted = tf.contrib.framework.sort(labels_pred, axis=-1, direction='ASCENDING', name='labels_pred_sorted')
            return labels_pred_sorted
        else:
            y_proba_argmax = tf.argmax(caps2_output_norm, axis=2, name="y_proba")
            y_pred = tf.squeeze(y_proba_argmax, name="y_pred")  # get predicted class by squeezing out irrelevant dimensions
            return y_pred


def compute_margin_loss(caps2_output_norm, labels, parameters):
    with tf.name_scope('margin_loss'):
        # the T in the margin equation
        if parameters.targ_and_flanks:  # there are two labels
            T = tf.reduce_sum(tf.one_hot(tf.cast(labels, tf.int64), depth=3), axis=1, name='T')
        else:
            T = tf.one_hot(tf.cast(labels, tf.int64), depth=parameters.caps2_ncaps, name="T")

        # present and absent errors go into the loss
        present_error_raw = tf.square(tf.maximum(0., parameters.m_plus - caps2_output_norm), name="present_error_raw")
        present_error = tf.reshape(present_error_raw, shape=(-1, parameters.caps2_ncaps), name="present_error")  # there is a term for each of the caps2ncaps possible outputs
        absent_error_raw = tf.square(tf.maximum(0., caps2_output_norm - parameters.m_minus), name="absent_error_raw")
        absent_error = tf.reshape(absent_error_raw, shape=(-1, parameters.caps2_ncaps), name="absent_error")    # there is a term for each of the caps2ncaps possible outputs

        # compute the margin loss
        L = tf.add(T * present_error, parameters.lambda_val * (1.0 - T) * absent_error, name="L")
        margin_loss = tf.reduce_mean(tf.reduce_sum(L, axis=1), name="margin_loss")

    return margin_loss


################################
#          Accuracy:           #
################################
def compute_accuracy(labels, labels_pred):
    with tf.name_scope('accuracy'):
        if parameters.targ_and_flanks:
            labels_reshaped = tf.reshape(labels, shape=[-1, 1])
            labels_pred_reshaped = tf.reshape(tf.cast(labels_pred, tf.int64), shape=[-1, 1])
            correct = tf.equal(labels_reshaped, labels_pred_reshaped, name='correct')
            accuracy = tf.reduce_mean(tf.cast(correct, tf.float32), name='accuracy')
        else:
            correct = tf.equal(labels, labels_pred, name='correct')
            accuracy = tf.reduce_mean(tf.cast(correct, tf.float32), name='accuracy')
        return accuracy


################################
#    Create decoder input:     #
################################
def create_masked_decoder_input(mask_with_labels, labels, labels_pred, caps2_output, parameters):
    with tf.name_scope('compute_decoder_input'):
        reconstruction_targets = tf.cond(mask_with_labels, lambda: labels, lambda: labels_pred, name='reconstruction_targets')
        # this conditional tests if one or more labels are passed
        reconstruction_mask = tf.cond(tf.rank(labels)>1, lambda: tf.reduce_sum(tf.one_hot(reconstruction_targets, depth=parameters.caps2_ncaps), axis=1, name='reconstruction_mask'), lambda: tf.one_hot(reconstruction_targets, depth=parameters.caps2_ncaps, name='reconstruction_mask'))
        reconstruction_mask = tf.reshape(reconstruction_mask, [-1, 1, parameters.caps2_ncaps, 1, 1], name='reconstruction_mask')
        caps2_output_masked = tf.multiply(caps2_output, reconstruction_mask, name='caps2_output_masked')
        
        # Flatten decoder inputs:
        decoder_input = tf.reshape(caps2_output_masked, [-1, parameters.caps2_ndims*parameters.caps2_ncaps], name='decoder_input')
        return decoder_input


################################
#     Vernieroffset loss:      #
################################
def compute_orientation_loss(target_caps_activation, labels):
    with tf.name_scope('compute_targori_loss'):

        depth = 2
        one_hot_offsets = tf.one_hot(tf.cast(labels, tf.int32), depth)
        targori_logits = tf.layers.dense(tf.squeeze(target_caps_activation), depth, activation=tf.nn.relu, name="offset_logits")
        targori_loss = tf.losses.softmax_cross_entropy(one_hot_offsets, targori_logits)
        targori_pred = tf.cast(tf.argmax(targori_logits, axis=1), tf.int64)
        correct = tf.equal(labels, targori_pred, name="correct")
        accuracy = tf.reduce_mean(tf.cast(correct, tf.float32), name="accuracy")
        tf.summary.scalar('accuracy', accuracy)

        return targori_pred, targori_loss


################################
#        sidesize loss:        #
################################
def compute_sidesize_loss(decoder_input, sidesizelabels, parameters, phase=True):
    with tf.name_scope('compute_sidesize_loss'):
        caps_activation = tf.squeeze(decoder_input)
        sidesizelabels = tf.squeeze(tf.cast(sidesizelabels, tf.int64))
        
        depth = 4
        T_sidesize = tf.one_hot(tf.cast(sidesizelabels, tf.int64), depth, name='T_sidesize')
        
        if parameters.batch_norm_sidesize:
            hidden_sidesize = tf.layers.dense(caps_activation, depth, use_bias=False, activation=None, reuse=tf.AUTO_REUSE, name='hidden_sidesize')
            hidden_sidesize = tf.layers.batch_normalization(hidden_sidesize, training=phase, reuse=tf.AUTO_REUSE, name='hidden_sidesize_bn')
        else:
            hidden_sidesize = tf.layers.dense(caps_activation, depth, activation=None, reuse=tf.AUTO_REUSE, name='hidden_sidesize')

        logits_sidesize = tf.nn.relu(hidden_sidesize, name='logits_sidesize')
        pred_sidesize = tf.argmax(logits_sidesize, axis=1, name='predicted_sidesize', output_type=tf.int64)
        squared_diff_sidesize = tf.square(tf.cast(sidesizelabels, tf.float32) - tf.cast(pred_sidesize, tf.float32), name='squared_diff_sidesize')
        tf.summary.histogram('sidesize_real', sidesizelabels)
        tf.summary.histogram('sidesize_pred', pred_sidesize)
        tf.summary.histogram('sidesize_distance', tf.sqrt(squared_diff_sidesize))
        correct_sidesize = tf.equal(sidesizelabels, pred_sidesize, name='correct_sidesize')
        accuracy_sidesize = tf.reduce_mean(tf.cast(correct_sidesize, tf.float32), name='accuracy_sidesize')
        
        if parameters.sidesize_loss == 'xentropy':
            loss_sidesize = tf.losses.softmax_cross_entropy(T_sidesize, logits_sidesize)
        elif parameters.sidesize_loss == 'squared_diff':
            loss_sidesize = tf.reduce_sum(squared_diff_sidesize, name='squared_diff_loss_sidesize')
        return loss_sidesize, accuracy_sidesize

################################
#        n_elements loss:      #
################################
def compute_n_elements_loss(decoder_input, n_elements_labels, parameters, phase=True):
    with tf.name_scope('compute_n_elements_loss'):
        caps_activation = tf.squeeze(decoder_input)
        n_elements_labels = tf.squeeze(tf.cast(n_elements_labels, tf.int64))

        depth = 25
        T_n_elements = tf.one_hot(tf.cast(n_elements_labels, tf.int64), depth, name='T_n_elements')

        if parameters.batch_norm_n_elements:
            hidden_n_elements = tf.layers.dense(caps_activation, depth, use_bias=False, activation=None, reuse=tf.AUTO_REUSE, name='hidden_n_elements')
            hidden_n_elements = tf.layers.batch_normalization(hidden_n_elements, training=phase, reuse=tf.AUTO_REUSE, name='hidden_n_elements_bn')
        else:
            hidden_n_elements = tf.layers.dense(caps_activation, depth, activation=None, reuse=tf.AUTO_REUSE, name='hidden_n_elements')

        logits_n_elements = tf.nn.relu(hidden_n_elements, name='logits_n_elements')
        pred_n_elements = tf.argmax(logits_n_elements, axis=1, name='predicted_n_elements', output_type=tf.int64)
        squared_diff_n_elements = tf.square(tf.cast(n_elements_labels, tf.float32) - tf.cast(pred_n_elements, tf.float32), name='squared_diff_n_elements')
        tf.summary.histogram('n_elements_real', n_elements_labels)
        tf.summary.histogram('n_elements_pred', pred_n_elements)
        tf.summary.histogram('n_elements_distance', tf.sqrt(squared_diff_n_elements))
        correct_n_elements = tf.equal(n_elements_labels, pred_n_elements, name='correct_n_elements')
        accuracy_n_elements = tf.reduce_mean(tf.cast(correct_n_elements, tf.float32), name='accuracy_n_elements')
        loss_n_elements = tf.losses.softmax_cross_entropy(T_n_elements, logits_n_elements)

        return loss_n_elements, accuracy_n_elements



################################
#       Location loss:         #
################################
def compute_location_loss(decoder_input, x_label, y_label, parameters, phase=True):
    with tf.name_scope('compute_location_loss'):
        caps_activation = tf.squeeze(decoder_input)
        
        # Loss for x-coordinates:
        x_label = tf.squeeze(x_label)
        x_depth = parameters.im_size[1]-parameters.patch_size
        T_x = tf.one_hot(tf.cast(x_label, tf.int64), x_depth, dtype=tf.float32, name='T_x')
        
        if parameters.batch_norm_location:
            hidden_x = tf.layers.dense(caps_activation, x_depth, use_bias=False, activation=None, name='hidden_x')
            hidden_x = tf.layers.batch_normalization(hidden_x, training=phase, name='hidden_x_bn')
        else:
            hidden_x = tf.layers.dense(caps_activation, x_depth, activation=None, name='hidden_x')
        
        x_logits = tf.nn.relu(hidden_x, name='x_logits')
        pred_x = tf.argmax(x_logits, axis=1, name='pred_x', output_type=tf.int64)
        x_squared_diff = tf.square(tf.cast(x_label, tf.float32) - tf.cast(pred_x, tf.float32), name='x_squared_difference_')
        tf.summary.histogram('x_real', x_label)
        tf.summary.histogram('x_pred', pred_x)
        tf.summary.histogram('x_distance', tf.sqrt(x_squared_diff))
        
        if parameters.location_loss == 'xentropy':
            x_loss = tf.losses.softmax_cross_entropy(T_x, x_logits)
        elif parameters.location_loss == 'squared_diff':
            x_loss = tf.reduce_sum(x_squared_diff, name='x_squared_difference_loss')


        # Loss for y-coordinates:
        y_label = tf.squeeze(y_label)
        y_depth = parameters.im_size[0]-parameters.patch_size
        T_y = tf.one_hot(tf.cast(y_label, tf.int64), y_depth, dtype=tf.float32, name='T_y_')
        
        if parameters.batch_norm_location:
            hidden_y = tf.layers.dense(caps_activation, y_depth, use_bias=False, activation=None, name='hidden_y')
            hidden_y = tf.layers.batch_normalization(hidden_y, training=phase, name='hidden_y_bn')
        else:
            hidden_y = tf.layers.dense(caps_activation, y_depth, activation=None, name='hidden_y')
        
        y_logits = tf.nn.relu(hidden_y, name='y_logits')
        pred_y = tf.argmax(y_logits, axis=1, name='pred_y', output_type=tf.int64)
        y_squared_diff = tf.square(tf.cast(y_label, tf.float32) - tf.cast(pred_y, tf.float32), name='y_squared_difference')
        tf.summary.histogram('y_real', y_label)
        tf.summary.histogram('y_pred', pred_y)
        tf.summary.histogram('y_distance', tf.sqrt(y_squared_diff))
        
        if parameters.location_loss == 'xentropy':
            y_loss = tf.losses.softmax_cross_entropy(T_y, y_logits)
        elif parameters.location_loss == 'squared_diff':
            y_loss = tf.reduce_sum(y_squared_diff, name='y_squared_difference_loss')

        return x_loss, y_loss


################################
#     Create reconstruction:   #
################################
def compute_reconstruction(decoder_input, parameters, phase=True, conv_output_size=None):
    # Finally comes the decoder (two dense fully connected ELU layers followed by a dense output sigmoid layer):
    with tf.name_scope('decoder_reconstruction'):
        # Lets do it as in the original paper:
        if parameters.rec_decoder_type == 'fc':
            # Hidden 1:
            if parameters.batch_norm_reconstruction:
                hidden_reconstruction_1 = tf.layers.dense(decoder_input, parameters.n_hidden_reconstruction_1,
                                                          use_bias=False,
                                                          reuse=tf.AUTO_REUSE, activation=None,
                                                          name='hidden_reconstruction_1')
                hidden_reconstruction_1 = tf.layers.batch_normalization(hidden_reconstruction_1, training=phase,
                                                                        reuse=tf.AUTO_REUSE,
                                                                        name='hidden_reconstruction_1_bn')
            else:
                hidden_reconstruction_1 = tf.layers.dense(decoder_input, parameters.n_hidden_reconstruction_1,
                                                          reuse=tf.AUTO_REUSE,
                                                          activation=None, name='hidden_reconstruction_1')
            hidden_reconstruction_1 = tf.nn.elu(hidden_reconstruction_1, name='hidden_reconstruction_1_activation')

            # Hidden 2:
            if parameters.batch_norm_reconstruction:
                hidden_reconstruction_2 = tf.layers.dense(hidden_reconstruction_1, parameters.n_hidden_reconstruction_2,
                                                          use_bias=False,
                                                          reuse=tf.AUTO_REUSE, activation=None,
                                                          name='hidden_reconstruction_2')
                hidden_reconstruction_2 = tf.layers.batch_normalization(hidden_reconstruction_2, training=phase,
                                                                        reuse=tf.AUTO_REUSE,
                                                                        name='hidden_reconstruction_2_bn')
            else:
                hidden_reconstruction_2 = tf.layers.dense(hidden_reconstruction_1, parameters.n_hidden_reconstruction_2,
                                                          reuse=tf.AUTO_REUSE,
                                                          activation=None, name='hidden_reconstruction_2')
            hidden_reconstruction_2 = tf.nn.elu(hidden_reconstruction_2, name='hidden_reconstruction_2_activation')

            reconstructed_output = tf.layers.dense(hidden_reconstruction_2, parameters.n_output, reuse=tf.AUTO_REUSE,
                                                   activation=tf.nn.sigmoid, name='reconstructed_output')

        # Lets deconvolute:
        elif parameters.rec_decoder_type == 'conv':
            # Redo step from primary caps (=conv3) to secondary caps (using fewer parameters):
            bottleneck_units = parameters.caps2_ncaps * parameters.caps2_ndims
            #            bottleneck_units = parameters.caps2_ncaps
            upsample_size1 = [conv_output_size[-1][1], conv_output_size[-1][2]]
            if parameters.batch_norm_reconstruction:
                upsample1 = tf.layers.dense(decoder_input, upsample_size1[0] * upsample_size1[1] * bottleneck_units,
                                            use_bias=False,
                                            activation=None, reuse=tf.AUTO_REUSE, name='upsample1_reconstruction')
                upsample1 = tf.layers.batch_normalization(upsample1, training=phase, reuse=tf.AUTO_REUSE,
                                                          name='upsample1_reconstruction_bn')
            else:
                upsample1 = tf.layers.dense(decoder_input, upsample_size1[0] * upsample_size1[1] * bottleneck_units,
                                            activation=None, reuse=tf.AUTO_REUSE, name='upsample1_reconstruction')
            upsample1 = tf.nn.elu(upsample1, name='upsample1_activation')
            upsample1 = tf.reshape(upsample1, [-1, upsample_size1[0], upsample_size1[1], bottleneck_units],
                                   name='reshaped_upsample_reconstruction')

            # Redo step from conv2 to primary caps (=conv3):
            upsample_size2 = [conv_output_size[-2][1], conv_output_size[-2][2]]
            upsample2 = tf.image.resize_images(upsample1, size=upsample_size2,
                                               method=tf.image.ResizeMethod.NEAREST_NEIGHBOR)
            if parameters.batch_norm_reconstruction:
                conv1_upsampled = tf.layers.conv2d(inputs=upsample2, filters=parameters.conv_params[-1]['filters'],
                                                   kernel_size=parameters.conv_params[-1]['kernel_size'],
                                                   use_bias=False, reuse=tf.AUTO_REUSE, padding='same', activation=None,
                                                   name='conv1_upsampled_reconstruction')
                conv1_upsampled = tf.layers.batch_normalization(conv1_upsampled, training=phase, reuse=tf.AUTO_REUSE,
                                                                name='conv1_upsampled_reconstruction_bn')
            else:
                conv1_upsampled = tf.layers.conv2d(inputs=upsample2, filters=parameters.conv_params[-1]['filters'],
                                                   kernel_size=parameters.conv_params[-1]['kernel_size'],
                                                   reuse=tf.AUTO_REUSE, padding='same', activation=None,
                                                   name='conv1_upsampled_reconstruction')
            conv1_upsampled = tf.nn.elu(conv1_upsampled, name='conv1_upsampled_reconstruction_activation')

            # Redo step from conv1 to conv2:
            upsample_size3 = [conv_output_size[-3][1], conv_output_size[-3][2]]
            upsample3 = tf.image.resize_images(conv1_upsampled, size=upsample_size3,
                                               method=tf.image.ResizeMethod.NEAREST_NEIGHBOR)
            if parameters.batch_norm_reconstruction:
                conv2_upsampled = tf.layers.conv2d(inputs=upsample3, filters=parameters.conv_params[-2]['filters'],
                                                   kernel_size=parameters.conv_params[-2]['kernel_size'],
                                                   use_bias=False, reuse=tf.AUTO_REUSE, padding='same', activation=None,
                                                   name='conv2_upsampled_reconstruction')
                conv2_upsampled = tf.layers.batch_normalization(conv2_upsampled, training=phase, reuse=tf.AUTO_REUSE,
                                                                name='conv2_upsampled_reconstruction_bn')
            else:
                conv2_upsampled = tf.layers.conv2d(inputs=upsample3, filters=parameters.conv_params[-2]['filters'],
                                                   kernel_size=parameters.conv_params[-2]['kernel_size'],
                                                   reuse=tf.AUTO_REUSE, padding='same', activation=None,
                                                   name='conv2_upsampled_reconstruction')
            conv2_upsampled = tf.nn.elu(conv2_upsampled, name='conv2_upsampled_reconstruction_activation')

            # Redo step from input image to conv1:
            upsample_size4 = parameters.im_size
            upsample4 = tf.image.resize_images(conv2_upsampled, size=(upsample_size4),
                                               method=tf.image.ResizeMethod.NEAREST_NEIGHBOR)
            if parameters.batch_norm_reconstruction:
                conv3_upsampled = tf.layers.conv2d(inputs=upsample4, filters=parameters.conv_params[-3]['filters'],
                                                   kernel_size=parameters.conv_params[-3]['kernel_size'],
                                                   use_bias=False, reuse=tf.AUTO_REUSE, padding='same', activation=None,
                                                   name='conv3_upsampled_reconstruction')
                conv3_upsampled = tf.layers.batch_normalization(conv3_upsampled, training=phase, reuse=tf.AUTO_REUSE,
                                                                name='conv3_upsampled_reconstruction_bn')
            else:
                conv3_upsampled = tf.layers.conv2d(inputs=upsample4, filters=parameters.conv_params[-3]['filters'],
                                                   kernel_size=parameters.conv_params[-3]['kernel_size'],
                                                   reuse=tf.AUTO_REUSE, padding='same', activation=None,
                                                   name='conv3_upsampled_reconstruction')
            conv3_upsampled = tf.nn.elu(conv3_upsampled, name='conv3_upsampled_reconstruction_activation')

            # Get back to greyscale
            reconstructed_output = tf.layers.conv2d(inputs=conv3_upsampled, filters=parameters.im_depth, kernel_size=1,
                                                    padding='same',
                                                    use_bias=True, reuse=tf.AUTO_REUSE, activation=tf.nn.sigmoid,
                                                    name='reconstructed_output')

            # Flatten the output to make it equal to the output of the fc
            reconstructed_output = tf.reshape(reconstructed_output, [-1, parameters.n_output],
                                              name='reconstructed_output_flat')

        else:
            raise SystemExit('\nPROBLEM: Your reconstruction decoder does not know what to do!\nCheck rec_decoder_type')

        return reconstructed_output


################################
#     Reconstruction loss:     #
################################
def compute_reconstruction_loss(X, reconstructed_output, parameters):
    with tf.name_scope('compute_reconstruction_loss'):
        imgs_flat = tf.reshape(X, [-1, parameters.n_output], name='imgs_flat')
        imgs_flat = tf.cast(imgs_flat, tf.float32)
        squared_difference = tf.square(imgs_flat - reconstructed_output, name='squared_difference')
        # reconstruction_loss = tf.reduce_mean(squared_difference, name='reconstruction_loss')
        reconstruction_loss = tf.reduce_sum(squared_difference, name='reconstruction_loss')
        return reconstruction_loss


def compute_shape_loss_cnn(cnn_activation, shapelabels, parameters, phase=True):
    '''
    Get predicted shape type loss for the cnn/rnn control network. These nets cannot use margin loss because they
    do not include capsules, so we use a decoder on the output layer instead.
    Parameters
    ----------
    cnn_activation: tensor
                    CNN final layer outputs
    shapelabels: tensor
                 Correct shapetype labels
    Returns
    -------
    labels_pred_sorted: tensor
                        List (of length n_labels) with numerically sorted predicted
                        shape types
    labels_pred_proba_sorted: tensor
                              List (of length n_labels) with corresponding probabilities
    '''
    with tf.name_scope('compute_shape_loss'):

        depth = parameters.caps2_ncaps  # this is the number of shape types
        n_labels = tf.cond(phase, lambda: 1, lambda: 2, name='n_labels') # one shape at a time during training, two shapes at a time during testing (orientation+flanker)
        T_shapelabels = tf.one_hot(tf.cast(shapelabels, tf.int64), depth, name='T_shapelabels')

        # Decide whether to use batch normalization
        hidden = tf.layers.dense(cnn_activation, depth, activation=None, name='hidden_shapeoffset')
        shapelabels_proba = tf.nn.relu(hidden, name='logits_shapelabels')
        top_labels_pred_proba, top_labels_pred = tf.nn.top_k(shapelabels_proba, n_labels, name='y_proba')
        top_ranked_shapelabels = tf.contrib.framework.sort(top_labels_pred_proba, direction='ASCENDING', name='ranked_shapelabels')
        top_labels_pred_sorted_idx = tf.contrib.framework.argsort(top_labels_pred, axis=-1, direction='ASCENDING', name='labels_pred_sorted')
        top_ranked_shapelabels_proba = tf.batch_gather(top_labels_pred_proba, top_labels_pred_sorted_idx)
        xent_shapelabels = tf.losses.softmax_cross_entropy(T_shapelabels, shapelabels_proba)  # if phase is True else 0.  # no need to compute loss during resting

        pred_shapelabels = tf.argmax(shapelabels_proba, axis=1, name='pred_shapelabels', output_type=tf.int64)
        correct_shapelabels = tf.equal(shapelabels, tf.cast(top_labels_pred, tf.int64), name='correct_shapelabels')
        accuracy_shapelabels = tf.reduce_mean(tf.cast(correct_shapelabels, tf.float32), name='accuracy_shapelabels')
        # return pred_shapelabels, xent_shapelabels, accuracy_shapelabels, top_ranked_shapelabels, top_ranked_shapelabels_proba
        return xent_shapelabels


def compute_orientation_loss_cnn(cnn_activation, targori_labels, parameters, phase=True):
    '''
    Create decoder for orientation in the CNN comparison neetwork and compute the loss as xentropy
    Parameters
    ----------
    cnn_activation: tensor
                    Outputs of the cnn
    targori_labels: tensor
                   Correct orientation labels
    parameters: flags
                Contains all parameters defined in parameters.py
    phase: bool
           If True, the decoder is in training mode
    Returns
    -------
    targori_pred: tensor
                        Predicted orientation offset labels
    targori_loss: tensor
                        orientation offset loss
    targori_accuracy: tensor
                            Accuracy for correctly predicting orientation offsets
    '''
    with tf.name_scope('compute_orientation_loss'):
        
        depth = 2
        targ_ori_labels_onehot = tf.one_hot(tf.cast(targori_labels, tf.int32), depth)
        targori_logits = tf.layers.dense(tf.squeeze(cnn_activation), depth, activation=None, name='targori_logits')  # marchait pas: activation=tf.nn.relu
        targori_loss = tf.losses.softmax_cross_entropy(targ_ori_labels_onehot, targori_logits)
        targori_pred = tf.argmax(targori_logits, axis=1, name='targori_pred', output_type=tf.int64)
        targori_correct = tf.equal(targori_labels, targori_pred, name='targori_correct')
        targori_accuracy = tf.reduce_mean(tf.cast(targori_correct, tf.float32), name='targori_accuracy')
        
        # tf.summary.scalar('targori_onehot', targ_ori_labels_onehot[0][0])
        # tf.summary.scalar('targori_logits_mean', tf.reduce_mean(targori_logits))
        # tf.summary.scalar('targori_pred', targori_pred[0])
        # tf.summary.scalar('targori_labels', targori_labels[0])
        # tf.summary.scalar('targori_cnnactivation', tf.reduce_max(cnn_activation))
        return targori_pred, targori_loss, targori_accuracy
        