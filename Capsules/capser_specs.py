from make_tf_data import make_stim_tfrecords
from parameters import parameters
import tensorflow as tf
import numpy
from capser_model_fn import model_fn
from capser_input_fn import predict_input_fn

###############################################
### Capsule network performance definitions ###
###############################################

nTrials = 480

logdir = parameters.data_path + '/_small_images/0/'
model = tf.estimator.Estimator(model_fn=model_fn, model_dir=logdir, params={'log_dir': logdir, 'trials': min(nTrials, parameters.batch_size)})

def compute_capser_performance(model, stim):

    make_stim_tfrecords(parameters.test_data_path, stim, max(nTrials, parameters.batch_size))

    if 0:
        import matplotlib.pyplot as plt
        with tf.Session() as sess:
            data_out, labels_out = predict_input_fn(parameters.test_data_path)
            img, labels = sess.run([data_out['images'], data_out['targori_label']])

            # Loop over each example in batch
            for i in range(2):
                plt.imshow(img[i, :, :, 0])
                plt.title('Class label = ' + str(labels[i]) + ', IM_SIZE=' + str(img.shape))
                plt.show()

    capser_out = list(model.predict(lambda: predict_input_fn(parameters.test_data_path)))
    targori_accuracy = [p['targori_accuracy'] for p in capser_out]

    return targori_accuracy[0]

# check_stim = -1*numpy.ones((15,19), dtype=numpy.int64)
check_stim = 1*numpy.ones((7,9), dtype=numpy.int64)
targori_accuracy = compute_capser_performance(model, check_stim)
print(targori_accuracy)
