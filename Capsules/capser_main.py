# -*- coding: utf-8 -*-
"""
Execute the training, evaluation and prediction of the capsnet
@author: Adrien Doerig based on code by Lynn Schmittwilken, who based her code on code by Adrien Doerig.
"""

import os
import logging
import numpy as np
os.environ['KMP_WARNINGS'] = 'off'
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "1"
import tensorflow as tf
tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)
from parameters import parameters
from capser_model_fn import capser_model_fn, cnn_model_fn
from capser_input_fn import train_input_fn, eval_input_fn, predict_input_fn
from capser_functions import save_params

###################################################


print('-----------------------------------------------------------')
print('TF version:', tf.__version__)
print('Starting capsnet training for densely cluttered displays.')
print('Check losses and accuracy: "tensorboard --logdir=./data"')
print('-----------------------------------------------------------')


###########################
#      Preparations:      #
###########################
# For reproducibility:
tf.reset_default_graph()
np.random.seed(42)
tf.set_random_seed(42)
n_iterations = parameters.n_iterations
model_fn = capser_model_fn if parameters.net_type.lower() == 'capsnet' else cnn_model_fn
for idx_execution in range(1, n_iterations):
    log_dir = parameters.logdir + str(idx_execution) + '/'
    
    ##################################
    #    Training and evaluation:    #
    ##################################
    # Output the loss in the terminal every few steps:
    # logging.getLogger().setLevel(logging.INFO)
    
    # Create the estimator:
    my_checkpointing_config = tf.estimator.RunConfig(keep_checkpoint_max=1)  # Retain the 2 most recent checkpoints.
    capser = tf.estimator.Estimator(model_fn=model_fn, model_dir=log_dir, config=my_checkpointing_config, params={'log_dir': log_dir})
    eval_spec = tf.estimator.EvalSpec(lambda: eval_input_fn(parameters.eval_data_path), start_delay_secs=0, steps=parameters.eval_steps, throttle_secs=parameters.eval_throttle_secs)

    # Lets go!
    for idx_round in range(1, parameters.n_rounds+1):
        train_spec = tf.estimator.TrainSpec(train_input_fn, max_steps=parameters.n_steps*idx_round)
        tf.estimator.train_and_evaluate(capser, train_spec, eval_spec)

print('... Finished capsnet script!')
print('-------------------------------------------------------')