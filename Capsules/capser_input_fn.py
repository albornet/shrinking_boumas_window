# -*- coding: utf-8 -*-
"""
Input fn to feed tfrecords files to the model
@author: Adrien Doerig based on code by Lynn Schmittwilken, who based her code on code by Adrien Doerig.
"""

import tensorflow as tf
from parameters import parameters


########################################
#     Parse tfrecords training set:    #
########################################
def parse_tfrecords_train(serialized_data):
    with tf.name_scope('Parsing_trainset'):
        # Define a dict with the data-names and types we expect to find in the TFRecords file.
        features = {'images': tf.FixedLenFeature([], tf.string),
                    'type_label': tf.FixedLenFeature([], tf.string),
                    'targori_label': tf.FixedLenFeature([], tf.string),
                    'sidesize_label': tf.FixedLenFeature([], tf.string),
                    'xpos_label': tf.FixedLenFeature([], tf.string),
                    'ypos_label': tf.FixedLenFeature([], tf.string),
                    'n_elements_label': tf.FixedLenFeature([], tf.string)}

        # Parse the serialized data so we get a dict with our data.
        parsed_data = tf.parse_single_example(serialized=serialized_data, features=features)
    
        # Get the raw bytes and decode to tf tensors.
        images = parsed_data['images']
        images = tf.decode_raw(images, tf.float32)
        images = tf.cast(images, tf.float32)
    
        type_label = parsed_data['type_label']
        type_label = tf.decode_raw(type_label, tf.int64)
        type_label = tf.cast(type_label, tf.int64)

        targori_label = parsed_data['targori_label']
        targori_label = tf.decode_raw(targori_label, tf.int64)
        targori_label = tf.cast(targori_label, tf.int64)

        sidesize_label = parsed_data['sidesize_label']
        sidesize_label = tf.decode_raw(sidesize_label, tf.int64)
        sidesize_label = tf.cast(sidesize_label, tf.int64)

        xpos_label = parsed_data['xpos_label']
        xpos_label = tf.decode_raw(xpos_label, tf.int64)
        xpos_label = tf.cast(xpos_label, tf.int64)

        ypos_label = parsed_data['ypos_label']
        ypos_label = tf.decode_raw(ypos_label, tf.int64)
        ypos_label = tf.cast(ypos_label, tf.int64)

        n_elements_label = parsed_data['n_elements_label']
        n_elements_label = tf.decode_raw(n_elements_label, tf.int64)
        n_elements_label = tf.cast(n_elements_label, tf.int64)

        # Reshaping:
        images = tf.reshape(images, [3, parameters.im_size[0], parameters.im_size[1], 1]) if parameters.separate_images else tf.reshape(images, [parameters.im_size[0], parameters.im_size[1], 1])
        if parameters.targ_and_flanks:
            type_label = tf.reshape(type_label, [2])
        else:
            type_label = tf.squeeze(tf.reshape(type_label, [1]))
        targori_label = tf.squeeze(tf.reshape(targori_label, [1]))
        sidesize_label = tf.squeeze(tf.reshape(sidesize_label, [1]))
        xpos_label = tf.squeeze(tf.reshape(xpos_label, [1]))
        ypos_label = tf.squeeze(tf.reshape(ypos_label, [1]))
        n_elements_label = tf.squeeze(tf.reshape(n_elements_label, [1]))

        return images, type_label, targori_label, sidesize_label, xpos_label, ypos_label, n_elements_label


###########################
#     Input function:     #
###########################
def input_fn(filenames, stage, parameters, buffer_size=1024):
    # Create a TensorFlow Dataset-object:
    dataset = tf.data.TFRecordDataset(filenames=filenames, num_parallel_reads=32)
    
    # Currently, I am using two different functions for parsing the train and 
    # test/eval set due to different data augmentation:
    if stage is 'train' or stage is 'eval':
        dataset = dataset.map(parse_tfrecords_train, num_parallel_calls=64)
        
        # Read a buffer of the given size and randomly shuffle it:
        dataset = dataset.shuffle(buffer_size=buffer_size)
        
        # Allow for infinite reading of data
        num_repeat = parameters.n_epochs


    elif stage is 'test':
        dataset = dataset.map(parse_tfrecords_train, num_parallel_calls=64)
        
        # Don't shuffle the data and only go through it once:
        num_repeat = 1

    # Repeat the dataset the given number of times and get a batch of data
    dataset = dataset.repeat(num_repeat)
    dataset = dataset.batch(parameters.batch_size, drop_remainder=True)
    
    # Use pipelining to speed up things (see https://www.youtube.com/watch?v=SxOsJPaxHME)
    dataset = dataset.prefetch(2)
    
    # Create an iterator for the dataset and the above modifications.
    iterator = dataset.make_one_shot_iterator()
    
    # Get the next batch of images and labels.
    [images, type_label, targori_label, sidesize_label, xpos_label, ypos_label, n_elements_label] = iterator.get_next()

    if stage is 'train':
        feed_dict = {'images': images,
                     'type_label': type_label,
                     'targori_label': targori_label,
                     'sidesize_label': sidesize_label,
                     'xpos_label': xpos_label,
                     'ypos_label': ypos_label,
                     'n_elements_label': n_elements_label,
                     'mask_with_labels': True,
                     'is_training': True}

    else:
        feed_dict = {'images': images,
                     'type_label': type_label,
                     'targori_label': targori_label,
                     'sidesize_label': sidesize_label,
                     'xpos_label': xpos_label,
                     'ypos_label': ypos_label,
                     'n_elements_label': n_elements_label,
                     'mask_with_labels': False,
                     'is_training': False}

    return feed_dict, type_label



##############################
#   Final input functions:   #
##############################
def train_input_fn():
    return input_fn(filenames=parameters.train_data_path, stage='train', parameters=parameters)

def eval_input_fn(filename):
    return input_fn(filenames=filename, stage='eval', parameters=parameters)

def predict_input_fn(filenames):
    return input_fn(filenames=filenames, stage='test', parameters=parameters)
