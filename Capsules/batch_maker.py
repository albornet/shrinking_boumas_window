# -*- coding: utf-8 -*-
"""
Creates input stimuli
@author: Adrien Doerig & Alban Bornet.
"""

import matplotlib.pyplot as plt
from parameters import parameters
import numpy as np
from scipy import ndimage




# Make a dataset to train the network
def make_train_dataset(n_samples=100, image_size=parameters.im_size, noise_level=parameters.train_noise,
                       stim_shape=parameters.stim_shape, max_size=parameters.max_size, min_size=parameters.min_size,
                       patch_size=parameters.patch_size, skip_elements=parameters.skip_elements, small_dataset=parameters.small_dataset,
                       targ_and_flanks=parameters.targ_and_flanks, jitter_only=parameters.jitter_only,
                       separate_images=parameters.separate_images, targ_ori_ampl=parameters.targ_ori_ampl, mode='train'):

    if small_dataset:
        image_size = (np.array(image_size)//1.4).astype(np.int64)
        patch_size = (np.array(patch_size)//1.4+1).astype(np.int64)
    data = np.zeros((n_samples, 3)+tuple(image_size), dtype=np.float32) if separate_images else np.zeros((n_samples,)+tuple(image_size), dtype=np.float32)
    if targ_and_flanks:
        type_label = np.zeros((n_samples,2), dtype=np.int64) # oriented target / horizontal square / vertical square
    else:
        type_label = np.zeros((n_samples,), dtype=np.int64) # oriented target / horizontal square / vertical square
    targori_label  = np.zeros((n_samples,), dtype=np.int64) # left or right
    sidesize_label = np.zeros((n_samples,), dtype=np.int64) # number of elements in a side of the square of elements
    n_elements_label = np.zeros((n_samples,), dtype=np.int64) # number of elements in a side of the square of elements
    xpos_label = np.zeros((n_samples,), dtype=np.int64) # x-slot-coord of top left corner for all 3 types
    ypos_label = np.zeros((n_samples,), dtype=np.int64) # y-slot-coord of top left corner for all 3 types
    n_flankers = 0 if mode is 'train' else stim_shape[0]*stim_shape[1]

    for n in range(n_samples):

        # 1 out of 2 stimuli are targets (to speed up target orientation learning)
        if targ_and_flanks:
            sample_type = np.random.randint(1,3)
        else:
            sample_type = np.random.randint(4)
            if sample_type < 2:
                sample_type = 0
            elif sample_type == 2:
                sample_type = 1
            else:
                sample_type = 2

        sample_skiprate   = np.random.uniform(skip_elements[0], skip_elements[1])
        sample_n_elements = 0
        sample_sidesize   = np.random.randint(min_size, max_size + 1) if sample_type > 0 else 1
        sample_targori    = np.random.randint(2)
        if targ_and_flanks:
            sample_xpos = np.random.randint(stim_shape[1] - sample_sidesize + 1)
            sample_ypos = np.random.randint(stim_shape[0] - sample_sidesize + 1)
            targ_xpos, targ_ypos, counter = sample_xpos, sample_ypos, 0
            while (sample_xpos <= targ_xpos <= sample_xpos+sample_sidesize-1) \
              and (sample_ypos <= targ_ypos <= sample_ypos+sample_sidesize-1) \
              and counter < 100:
                targ_xpos = np.random.randint(stim_shape[1])
                targ_ypos = np.random.randint(stim_shape[0])
                counter  += 1
        else:
            if jitter_only:
                if sample_type == 0:
                    sample_xpos = np.random.randint(stim_shape[1]//2-2, stim_shape[1]//2+3)
                    sample_ypos = np.random.randint(stim_shape[0]//2-2, stim_shape[0]//2+3)
                    # [sample_ypos, sample_xpos] = parameters.targ_pos
                else:
                    jitter      = min(1, (min(stim_shape) - sample_sidesize)//2)
                    sample_xpos = np.random.randint(stim_shape[1]//2-jitter, stim_shape[1]//2+jitter+1)
                    sample_ypos = np.random.randint(stim_shape[0]//2-jitter, stim_shape[0]//2+jitter+1)
            else:
                if sample_type == 0:
                    sample_xpos = np.random.randint(stim_shape[1]-sample_sidesize + 1)
                    sample_ypos = np.random.randint(stim_shape[0]-sample_sidesize + 1)
                    # [sample_ypos, sample_xpos] = parameters.targ_pos
                else:
                    sample_xpos = np.random.randint(stim_shape[1]-sample_sidesize + 1)
                    sample_ypos = np.random.randint(stim_shape[0]-sample_sidesize + 1)

        stimulus = np.zeros((2,)+tuple([s*patch_size for s in stim_shape]))

        if n_flankers > 0:
            flank_poses = np.random.choice(stim_shape[0]*stim_shape[1], n_flankers, replace=False)
            for flank_pos in flank_poses:
                flank_pos = np.unravel_index(flank_pos, stim_shape)  # [np.random.randint(0, high) for high in stim_shape]
                # if not (flank_pos[0] == targ_pos[0] and flank_pos[1] == targ_pos[1]):
                flank_ori = 90*np.random.choice(2, 1, p=[0.3, 0.7])[0]  # 70% horizontals
                stimulus[1, flank_pos[0]*patch_size:(flank_pos[0]+1)*patch_size, flank_pos[1]*patch_size:(flank_pos[1]+1)*patch_size] = create_patch(flank_ori, patch_size)

        if mode is 'train':
            this_targ_ori_ampl = np.random.uniform(targ_ori_ampl/2, targ_ori_ampl*2)
            if sample_type == 0:
                sample_n_elements += 1
                targ_ori = (sample_targori - 0.5) * 2 * this_targ_ori_ampl  # -10 or 10 degrees
                stimulus[0, sample_ypos*patch_size:(sample_ypos+1)*patch_size, sample_xpos*patch_size:(sample_xpos+1)*patch_size] = create_patch(targ_ori, patch_size)
            
            if targ_and_flanks:
                sample_n_elements += 1
                targ_ori = (sample_targori - 0.5) * 2 * this_targ_ori_ampl  # -10 or 10 degrees
                stimulus[0, targ_ypos*patch_size:(targ_ypos+1)*patch_size, targ_xpos*patch_size:(targ_xpos+1)*patch_size] = create_patch(targ_ori, patch_size)

            if sample_type != 0 or targ_and_flanks:
                sample_grid = np.random.choice([0,1], size=(sample_sidesize, sample_sidesize), p=[sample_skiprate, 1-sample_skiprate])
                for i in range(sample_sidesize):
                    for j in range(sample_sidesize):
                        sample_n_elements += 1
                        xpos = sample_xpos-sample_sidesize//2+j
                        ypos = sample_ypos-sample_sidesize//2+i
                        stimulus[1, ypos*patch_size:(ypos+1)*patch_size, xpos*patch_size:(xpos+1)*patch_size] = create_patch((sample_type-1)*90, patch_size)

        elif mode is 'eval':
            [sample_ypos, sample_xpos] = parameters.targ_pos
            targ_ori = (sample_targori - 0.5) * 2 * targ_ori_ampl  # -10 or 10 degrees
            stimulus[0, sample_ypos * patch_size:(sample_ypos + 1) * patch_size, sample_xpos * patch_size:(sample_xpos + 1) * patch_size] = create_patch(targ_ori, patch_size)
            stimulus[1, sample_ypos * patch_size:(sample_ypos + 1) * patch_size, sample_xpos * patch_size:(sample_xpos + 1) * patch_size] = np.zeros(patch_size)

        if parameters.separate_images:
            firstRow = int((image_size[0] - stimulus.shape[1]) / 2)  # + np.random.randint(-5,6)
            firstCol = int((image_size[1] - stimulus.shape[2]) / 2)  # + np.random.randint(-5,6)
            data[n, :2, firstRow:firstRow + stimulus.shape[1], firstCol:firstCol + stimulus.shape[2]] = stimulus
            data[n,  2, firstRow:firstRow + stimulus.shape[1], firstCol:firstCol + stimulus.shape[2]] = np.sum(stimulus, axis=0)
        else:
            firstRow = int((image_size[0] - stimulus.shape[1]) / 2)  # + np.random.randint(-5,6)
            firstCol = int((image_size[1] - stimulus.shape[2]) / 2)  # + np.random.randint(-5,6)
            data[n, firstRow:firstRow+stimulus.shape[1], firstCol:firstCol+stimulus.shape[2]] = np.sum(stimulus, axis=0)

        if targ_and_flanks:
            type_label[n,:] = [0, sample_type]
        else:
            type_label[n] = sample_type
        targori_label[n]    = sample_targori
        sidesize_label[n]   = sample_sidesize-2  # we want to have labels 0->3 for 2->5 sidesize (because we will use crowssentropy loss)
        n_elements_label[n] = sample_n_elements
        xpos_label[n]       = targ_xpos if targ_and_flanks else sample_xpos
        ypos_label[n]       = targ_ypos if targ_and_flanks else sample_ypos

    added_noise = np.random.normal(0, noise_level, data.shape)
    data += added_noise
    data  = (data-data.min())/(data.max()-data.min())
    data  = np.expand_dims(data,-1)

    if 0:
        for i in range(min(5,n_samples)):
            if parameters.separate_images:
                for this_im in range(3):
                    plt.subplot(1,3,this_im+1)
                    plt.imshow(data[i,this_im,:,:,0])
            else:
                plt.figure()
                plt.imshow(data[i,:,:,0])
            plt.title('type: ' + str(type_label[i]) + ', targori_label: ' + str(targori_label[i])+ ', sidesize_label: ' + str(sidesize_label[i]) + ', [xpos, ypos]: [' + str(xpos_label[i]) + ', ' + str(ypos_label[i]) + '], n_elements_label: ' + str(n_elements_label[i]))
            plt.show()

    return data, type_label, targori_label, sidesize_label, xpos_label, ypos_label, n_elements_label



# Create patch with defined orientation
def create_patch(ori, patch_size, ampl=1.0):

    space = 1
    patch = np.zeros((patch_size, patch_size))
    patch[space:-1-(space-1), int((patch_size-1)/2)] = ampl
    patch = ndimage.rotate(patch, ori, order=1, reshape=False)

    return patch


# To use later in the GA (something like that)
# Generate a batch of displays that will be fed to alexnet to compute the performance of the DNN
def make_config_batch(stim, n_samples, targ_pos=parameters.targ_pos, image_size=parameters.im_size,
    noise_level=parameters.eval_noise, targ_ori_ampl=parameters.targ_ori_ampl, patch_size=parameters.patch_size):

    # we only care about the targori_label, but the network expects all inputs, so we fill the other labels too, randomly
    data             = np.zeros((n_samples,)+tuple(image_size), dtype=np.float32)
    type_label       = np.zeros((n_samples,), dtype=np.int64)  # oriented target / horizontal square / vertical square
    targori_label    = np.zeros((n_samples,), dtype=np.int64)  # left or right
    max_size         = 7                                       # max number of elements in a side of the square of elements
    min_size         = 2                                       # min number of elements in a side of the square of elements
    sidesize_label   = np.zeros((n_samples,), dtype=np.int64)  # number of elements in a side of the square of elements
    xpos_label       = np.zeros((n_samples,), dtype=np.int64)  # x-slot-coord of top left corner for all 3 types
    ypos_label       = np.zeros((n_samples,), dtype=np.int64)  # y-slot-coord of top left corner for all 3 types
    n_elements_label = np.zeros((n_samples,), dtype=np.int64)  # y-slot-coord of top left corner for all 3 types
    ampl             = 1.0  #  2.0 if np.abs(stim).sum() > 10 else 1.0
    stimulus         = np.zeros(tuple([s*patch_size for s in stim.shape]))
    for n in range(n_samples):

        sample_n_elements = 0
        sample_targori    = np.random.randint(2)
        targ_ori          = (sample_targori-0.5)*2*targ_ori_ampl
        targori_label[n]  = (1-np.sign(targ_ori))/2  # L->0, R->1

        for i in range(stim.shape[0]):
            for j in range(stim.shape[1]):
                if stim[i,j] != 0:  # 0 means no element at position (i,j)
                    flank_ori = 90 if stim[i,j] == -1 else 0
                    stimulus[i*patch_size:(i+1)*patch_size, j*patch_size:(j+1)*patch_size] = create_patch(flank_ori, patch_size, ampl)
                    sample_n_elements += 1

        stimulus[targ_pos[0]*patch_size:(targ_pos[0]+1)*patch_size, targ_pos[1]*patch_size:(targ_pos[1]+1)*patch_size] = create_patch(targ_ori, patch_size, ampl)
        firstRow = int((image_size[0]-stimulus.shape[0])/2)
        firstCol = int((image_size[1]-stimulus.shape[1])/2)
        data[n, firstRow:firstRow + stimulus.shape[0], firstCol:firstCol + stimulus.shape[1]] = stimulus

        type_label[n]       = np.random.randint(3)
        targori_label[n]    = sample_targori
        sidesize_label[n]   = np.random.randint(min_size, max_size + 1) - 2  # we want to have labels 0->3 for 2->5 sidesize (because we will use crowssentropy loss)
        xpos_label[n]       = np.random.randint(3, 6)
        ypos_label[n]       = np.random.randint(2, 5)
        n_elements_label[n] = sample_n_elements

    if noise_level > 0.0:
        added_noise = np.random.normal(0, noise_level, data.shape)
        data       += added_noise
        data        = (data - data.min()) / (data.max() - data.min())
    data        = np.expand_dims(data, -1)
    
    if 0:
        for i in range(min(n_samples, 5)):
            plt.figure()
            plt.imshow(data[i,:,:,0])
            plt.title('type: ' + str(type_label[i]) + ', targori_label: ' + str(targori_label[i])+ ', sidesize_label: ' + str(sidesize_label[i]) + ', [xpos, ypos]: [' + str(xpos_label[i]) + ', ' + str(ypos_label[i]) + '].')
            plt.show()

    if parameters.separate_images:
        data = np.tile(np.expand_dims(data,1), (1,3,1,1,1))  # for testing, we don't need separate images for the target and flanekrs, but we still need the same shape as during training
    
    return data, type_label, targori_label, sidesize_label, xpos_label, ypos_label, n_elements_label