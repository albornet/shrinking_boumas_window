# -*- coding: utf-8 -*-
"""
My script to create tfrecords files based on batchmaker class
@author: Adrien Doerig based on code by Lynn Schmittwilken, who based her code on code by Adrien Doerig.
"""

import sys
import os
import tensorflow as tf
from parameters import parameters
from batch_maker import make_train_dataset, make_config_batch


##################################
#       Helper functions:        #
##################################
def wrap_int64(value):
    output = tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))
    return output

def wrap_bytes(value):
    output = tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))
    return output

def print_progress(count, total):
    percent_complete = float(count) / total
    msg = "\r- Progress: {0:.1%}".format(percent_complete)
    # Print it.
    sys.stdout.write(msg)
    sys.stdout.flush()


##################################
#      tfrecords function:       #
##################################
def make_tfrecords(out_path, mode, n_samples):
    '''Function to create tfrecord files based on stim_maker class'''
    # Inputs:
    # mode: decide whether to create the training (=training) or testing (=testing) dataset;
    # n_samples: samples in the dataset
    
    print("\nConverting: " + out_path)
    
    # Open a TFRecordWriter for the output-file.
    with tf.io.TFRecordWriter(out_path) as writer:

        # Create images one by one using stimMaker and save them
        for i in range(n_samples):
            print_progress(count=i, total=n_samples - 1)
            
            # Either create training or testing dataset
            noise = parameters.train_noise if mode is 'train' else parameters.eval_noise
            [images, type_label, targori_label, sidesize_label,  xpos_label, ypos_label, n_elements_label] = make_train_dataset(n_samples=1, noise_level=noise, mode=mode)


            # Convert the image to raw bytes.
            images_bytes     = images.tostring()
            type_label_bytes = type_label.tostring()
            targori_label_bytes = targori_label.tostring()
            sidesize_label_bytes = sidesize_label.tostring()
            xpos_label_bytes = xpos_label.tostring()
            ypos_label_bytes = ypos_label.tostring()
            n_elements_label_bytes = n_elements_label.tostring()


            # Create a dict with the data to save in the TFRecords file
            data = {'images': wrap_bytes(images_bytes),
                    'type_label': wrap_bytes(type_label_bytes),
                    'targori_label': wrap_bytes(targori_label_bytes),
                    'sidesize_label': wrap_bytes(sidesize_label_bytes),
                    'xpos_label': wrap_bytes(xpos_label_bytes),
                    'ypos_label': wrap_bytes(ypos_label_bytes),
                    'n_elements_label': wrap_bytes(n_elements_label_bytes)}

            # Wrap the data as TensorFlow Features.
            feature = tf.train.Features(feature=data)

            # Wrap again as a TensorFlow Example.
            example = tf.train.Example(features=feature)

            # Serialize the data.
            serialized = example.SerializeToString()

            # Write the serialized data to the TFRecords file.
            writer.write(serialized)
    return


def make_stim_tfrecords(out_path, stim, n_samples):
    '''Function to create tfrecord files for a stimulus given as input based on stim_maker class'''
    # Inputs:
    # stim: a 2D array of -1 for horizontal and 1 for vertical bars;
    # n_samples: samples in the dataset

    # Open a TFRecordWriter for the output-file.
    with tf.python_io.TFRecordWriter(out_path) as writer:
        # Create images one by one using stimMaker and save them
        for i in range(n_samples):

            [images, type_label, targori_label, sidesize_label, xpos_label, ypos_label, n_elements_label] = make_config_batch(stim, 1)

            # Convert the image to raw bytes.
            images_bytes = images.tostring()
            type_label_bytes = type_label.tostring()
            targori_label_bytes = targori_label.tostring()
            sidesize_label_bytes = sidesize_label.tostring()
            xpos_label_bytes = xpos_label.tostring()
            ypos_label_bytes = ypos_label.tostring()
            n_elements_label_bytes = n_elements_label.tostring()

            # Create a dict with the data to save in the TFRecords file
            data = {'images': wrap_bytes(images_bytes),
                    'type_label': wrap_bytes(type_label_bytes),
                    'targori_label': wrap_bytes(targori_label_bytes),
                    'sidesize_label': wrap_bytes(sidesize_label_bytes),
                    'xpos_label': wrap_bytes(xpos_label_bytes),
                    'ypos_label': wrap_bytes(ypos_label_bytes),
                    'n_elements_label': wrap_bytes(n_elements_label_bytes)}

            # Wrap the data as TensorFlow Features.
            feature = tf.train.Features(feature=data)

            # Wrap again as a TensorFlow Example.
            example = tf.train.Example(features=feature)

            # Serialize the data.
            serialized = example.SerializeToString()

            # Write the serialized data to the TFRecords file.
            writer.write(serialized)
    return


###################################
#     Create tfrecords files:     #
###################################

if __name__ == '__main__':

    train = 1
    valid = 1
    if not os.path.exists(parameters.data_path):
        os.mkdir(parameters.data_path)

    # Create the training set:
    if train:
        print('\n-------------------------------------------------------')
        print('Creating tfrecords training files...')
        mode = 'train'
        make_tfrecords(parameters.train_data_path, mode, parameters.n_train_samples)
        print('\n-------------------------------------------------------')
        print('Finished creation of training set')
        print('-------------------------------------------------------')


    # Create the eval set:
    if valid:
        print('\n-------------------------------------------------------')
        print('Creating tfrecords evaluation files...')
        mode = 'eval'
        make_tfrecords(parameters.eval_data_path, mode, parameters.n_eval_samples)
        print('\n-------------------------------------------------------')
        print('Finished creation of eval set')
        print('-------------------------------------------------------')

