# -*- coding: utf-8 -*-
"""
model_fn for tf train_and_evaluate API
All functions that are called in this script are described in more detail in
capser_functions.py
@author: Adrien Doerig based on code by Lynn Schmittwilken, who based her code on code by Adrien Doerig.
"""

import tensorflow as tf
import numpy as np

from parameters import parameters
from capser_functions import *


def capser_model_fn(features, labels, mode, params):
    # features: This is the x-arg from the input_fn.
    # labels:   This is the y-arg from the input_fn.
    # mode:     Either TRAIN, EVAL, or PREDICT
    # params:   Optional parameters; here not needed because of parameter-file
    
    plot_n_images = 4
    log_dir = params['log_dir']
    
    ##########################################
    #      Prepararing input variables:      #
    ##########################################
    images = tf.cast(features['images'], tf.float32)
    full_images = images[:,2,:,:,:] if parameters.separate_images else images  # full_images contains target+flankers. If parameters.separate_images = True, images contains 3 images: target alone, flankers alone and full_image
    type_label = tf.cast(features['type_label'], tf.int64)
    targori_label = tf.cast(features['targori_label'], tf.int64)
    sidesize_label = tf.cast(features['sidesize_label'], tf.int64)
    xpos_label = tf.cast(features['xpos_label'], tf.int64)
    ypos_label = tf.cast(features['ypos_label'], tf.int64)
    n_elements_label = tf.cast(features['n_elements_label'], tf.int64)
    mask_with_labels = tf.placeholder_with_default(features['mask_with_labels'], shape=(), name='mask_with_labels')
    is_training = tf.placeholder_with_default(features['is_training'], shape=(), name='is_training')
    
    tf.summary.image('input_images', full_images, plot_n_images)
    if parameters.separate_images and 0:  # set to 1 to see separate images in tensorboard
        tf.summary.image('target_input_images', images[:,0,:,:,:], plot_n_images)
        tf.summary.image('flanker_input_images', images[:,1,:,:,:], plot_n_images)
    batch_size = parameters.batch_size


    ##########################################
    #          Build the capsnet:            #
    ##########################################

    conv_output, conv_output_sizes = conv_layers(full_images, parameters, is_training)
    caps1_output = primary_caps_layer(conv_output, parameters)
    caps2_output, caps2_output_norm = secondary_caps_layer(caps1_output, batch_size, parameters)
    with tf.name_scope('1_predicted_type'):
        type_pred = predict_type_label(caps2_output_norm)
        type_accuracy = compute_accuracy(type_label, type_pred)
        tf.summary.scalar('type_accuracy', type_accuracy)
    
    
    #####################################################################
    #     Create masked decoder input and decode target orientation     #
    #####################################################################

    with tf.name_scope('2_target_ori_acuity'):
        target_caps_activation = caps2_output[:, :, 0, :, :]
        target_caps_activation = tf.expand_dims(target_caps_activation, 2)
        targori_pred, targori_loss = compute_orientation_loss(target_caps_activation, targori_label)
        targori_loss *= parameters.alpha_targori
        tf.summary.scalar('targori_loss', targori_loss)

    ##########################################
    #            Prediction mode:            #
    ##########################################
    if mode == tf.estimator.ModeKeys.PREDICT:
        targori_accuracy = compute_accuracy(targori_label[:params['trials']], targori_pred[:params['trials']])
        predictions = {'targori_accuracy': tf.ones(shape=batch_size) * targori_accuracy}  # need batch_size items to return.
        spec = tf.estimator.EstimatorSpec(mode=mode, predictions=predictions)


    ##########################################
    #       Train or Evaluation mode:        #
    ##########################################
    else:        

        with tf.name_scope('3_margin'):
            margin_loss = compute_margin_loss(caps2_output_norm, type_label, parameters)
            margin_loss = parameters.alpha_margin * margin_loss
            tf.summary.scalar('margin_loss', margin_loss)

        with tf.name_scope('create_masked_decoder_input'):
            if parameters.decode_sidesize or parameters.decode_location or parameters.decode_n_elements or parameters.reconstruct:
                masked_decoder_input = create_masked_decoder_input(mask_with_labels, type_label, type_pred, caps2_output, parameters)

        with tf.name_scope('4_sidesize'):
            if parameters.decode_sidesize:
                sidesize_loss, sidesize_accuracy = compute_sidesize_loss(masked_decoder_input, sidesize_label, parameters, is_training)
                sidesize_loss *= parameters.alpha_sidesize
            else:
                sidesize_loss, sidesize_accuracy = 0., 0.
            tf.summary.scalar('sidesize_loss', sidesize_loss)
            tf.summary.scalar('sidesize_accuracy', sidesize_accuracy)

        with tf.name_scope('5_location'):
            if parameters.decode_location:
                xpos_loss, ypos_loss = compute_location_loss(masked_decoder_input, xpos_label, ypos_label, parameters, is_training)
                xpos_loss = parameters.alpha_xpos_loss * xpos_loss
                ypos_loss = parameters.alpha_ypos_loss * ypos_loss
            else:
                xpos_loss = 0.
                ypos_loss = 0.

            location_loss = xpos_loss + ypos_loss
            tf.summary.scalar('xpos_loss', xpos_loss)
            tf.summary.scalar('ypos_loss', ypos_loss)

        with tf.name_scope('6_n_elements'):
            if parameters.decode_n_elements:
                n_elements_loss, n_elements_accuracy = compute_n_elements_loss(masked_decoder_input, n_elements_label, parameters, is_training)
                n_elements_loss *= parameters.alpha_n_elements
            else:
                n_elements_loss, n_elements_accuracy = 0., 0.
            tf.summary.scalar('n_elements_loss', n_elements_loss)
            tf.summary.scalar('n_elements_accuracy', n_elements_accuracy)

        with tf.name_scope('7_reconstruction'):
            if parameters.reconstruct:
                if parameters.separate_images:  # in this case, we compute reconstructions from each capsule, sum errors and show them in different colors
                    decoder_outputs = []
                    reconstruction_loss = 0
                    for this_capsule in range(2):
                        # Create decoder outputs for shape_1 images batch
                        this_capsule_masked_decoder_input = create_masked_decoder_input(mask_with_labels, type_label[:,this_capsule], type_pred[:,this_capsule], caps2_output, parameters)
                        this_capsule_reconstruction = compute_reconstruction(this_capsule_masked_decoder_input, parameters, is_training, conv_output_sizes)
                        reconstruction_loss += compute_reconstruction_loss(images[:,this_capsule,:,:,:], this_capsule_reconstruction, parameters)
                        decoder_outputs.append(this_capsule_reconstruction)
                    reconstruction_loss *= parameters.alpha_reconstruction
                    tf.summary.scalar('reconstruction_loss', reconstruction_loss)

                    decoder_outputs = tf.stack(decoder_outputs)  # put decoded images for each shape in the same tensor
                    decoder_outputs = tf.transpose(decoder_outputs, [1, 2, 0])  # technicality. don't worry.
                    # make an rgb tf.summary image. Note: there's some fucked up dimension tweaking but it works. Don't worry about details.
                    color_masks = np.array([[121, 199, 83],  # 0: vernier, green
                                            [220, 76, 70],  # 1: red
                                            [79, 132, 196]])  # 3: blue
                    color_masks = np.expand_dims(color_masks, axis=1)
                    color_masks = np.expand_dims(color_masks, axis=1)
                    decoder_output_images = tf.reshape(decoder_outputs, [-1, parameters.im_size[0], parameters.im_size[1], 2])
                    decoder_output_images_rgb_0 = tf.image.grayscale_to_rgb(
                        tf.expand_dims(decoder_output_images[:, :, :, 0], axis=-1)) * color_masks[0, :, :, :]
                    decoder_output_images_rgb_1 = tf.image.grayscale_to_rgb(
                        tf.expand_dims(decoder_output_images[:, :, :, 1], axis=-1)) * color_masks[1, :, :, :]
                    decoder_output_images_sum = decoder_output_images_rgb_0 + decoder_output_images_rgb_1
                    tf.summary.image('decoder_output_img', decoder_output_images_sum, plot_n_images)

                else:
                    # Create decoder outputs for shape_1 images batch
                    reconstruction = compute_reconstruction(masked_decoder_input, parameters, is_training, conv_output_sizes)
                    # Calculate reconstruction loss for shape_1 images batch
                    reconstruction_loss = compute_reconstruction_loss(full_images, reconstruction, parameters)
                    reconstruction_loss *= parameters.alpha_reconstruction
                    tf.summary.scalar('reconstruction_loss', reconstruction_loss)
                    reconstruction_img = tf.reshape(reconstruction, [batch_size, parameters.im_size[0], parameters.im_size[1], 1], name='shape_1_img_reconstructed')
                    tf.summary.image('decoder_output_img', reconstruction_img, plot_n_images)

            else:
                reconstruction_loss = 0.0


    ##########################################
    #              Final loss                #
    ##########################################
        final_loss = tf.add_n([margin_loss, targori_loss, sidesize_loss, location_loss, reconstruction_loss], name='final_loss')


    ##########################################
    #        Training operations             #
    ##########################################
        # The following is needed due to how tf.layers.batch_normalzation works:
        update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
        with tf.control_dependencies(update_ops):
            # learning_rate = tf.train.cosine_decay_restarts(parameters.learning_rate, tf.train.get_global_step(),
            #                                                parameters.learning_rate_decay_steps, name='learning_rate')
            learning_rate = tf.train.cosine_decay_restarts(parameters.learning_rate, tf.train.get_global_step(),
                                                           parameters.learning_rate_decay_steps, alpha=0.4, name='learning_rate')
            optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
            train_op = optimizer.minimize(loss=final_loss, global_step=tf.train.get_global_step(), name='train_op')
            tf.summary.scalar('learning_rate', learning_rate)
        
        # write summaries during evaluation
        eval_summary_hook = tf.train.SummarySaverHook(save_steps=100,
                                                      output_dir=log_dir + '/eval',
                                                      summary_op=tf.summary.merge_all())
        
        # Wrap all of this in an EstimatorSpec.
        # print_hook = tf.train.LoggingTensorHook({"type:": type_label, "targori:": targori_label, "sidesize:": sidesize_label, "xpos:": xpos_label, "ypos:": ypos_label}, every_n_iter=10)
        spec = tf.estimator.EstimatorSpec(
            mode=mode,
            loss=final_loss,
            train_op=train_op,
            eval_metric_ops={},
            # training_hooks=[print_hook],
            evaluation_hooks=[eval_summary_hook])
    
    return spec




# model function for control networks (ffCNN, lateral recurrence in last layer or top-down recurrence)
# it is almost the same function as the capsnet model function, except for the two capsule layers, which
# can be feedforward or recurrent fully-connected layers. Two of the decoders need to be slightly changed
# to deal with FC layers instead off capsules. Otherwise there is no difference.
def cnn_model_fn(features, labels, mode, params):
    # features: This is the x-arg from the input_fn.
    # labels:   This is the y-arg from the input_fn.
    # mode:     Either TRAIN, EVAL, or PREDICT
    # params:   Optional parameters; here not needed because of parameter-file

    plot_n_images = 4
    log_dir = params['log_dir']
    
    ##########################################
    #      Prepararing input variables:      #
    ##########################################
    images = tf.cast(features['images'], tf.float32)
    full_images = images[:,2,:,:,:] if parameters.separate_images else images  # full_images contains target+flankers. If parameters.separate_images = True, images contains 3 images: target alone, flankers alone and full_image
    type_label = tf.cast(features['type_label'], tf.int64)
    targori_label = tf.cast(features['targori_label'], tf.int64)
    sidesize_label = tf.cast(features['sidesize_label'], tf.int64)
    xpos_label = tf.cast(features['xpos_label'], tf.int64)
    ypos_label = tf.cast(features['ypos_label'], tf.int64)
    n_elements_label = tf.cast(features['n_elements_label'], tf.int64)
    mask_with_labels = tf.placeholder_with_default(features['mask_with_labels'], shape=(), name='mask_with_labels')
    is_training = tf.placeholder_with_default(features['is_training'], shape=(), name='is_training')
    
    tf.summary.image('input_images', full_images, plot_n_images)
    if parameters.separate_images and 0:  # set to 1 to see separate images in tensorboard
        tf.summary.image('target_input_images', images[:,0,:,:,:], plot_n_images)
        tf.summary.image('flanker_input_images', images[:,1,:,:,:], plot_n_images)
    batch_size = parameters.batch_size


    ##########################################
    #          Build the capsnet:            #
    ##########################################

    conv_output, conv_output_sizes = conv_layers(full_images, parameters, is_training)

    # Create a standard fully connected layer with as many parameters as the caps2 layer of the capsnet.
    # Also, we need the individual outputs of the vernier caps and shape caps
    flat_conv_output = tf.layers.flatten(conv_output, name='flat_conv_output')

    # CNN VERSION // same neumber of neurons as the CapsNet
    if parameters.net_type == 'CNN':
        flat_conv_output = tf.nn.elu(flat_conv_output)
        cnn_output = tf.layers.dense(flat_conv_output, parameters.caps2_ncaps * parameters.caps2_ndims, use_bias=True,
                                     activation=tf.nn.elu, name='fc_layer')

    # LATERAL RNN VERSION // there is no simple RNN class that we can use in tf.Estimators, so instead we repeatedly use the same fc layer with the same parameters
    elif parameters.net_type == 'LATERAL_RNN':
        with tf.variable_scope('LATERAL_RNN'):
            bottom_layer = tf.nn.elu(flat_conv_output)
            bottom_up_input = tf.layers.dense(bottom_layer, parameters.caps2_ncaps * parameters.caps2_ndims,
                                              use_bias=True, activation=None, name='bottum_up')
            recurrent_state = tf.nn.elu(bottom_up_input)
        for i in range(parameters.iter_routing - 1):  # -1 because the first iteration is above
            with tf.variable_scope('LATERAL_RNN', reuse=tf.AUTO_REUSE):
                recurrent_input = tf.layers.dense(recurrent_state, parameters.caps2_ncaps * parameters.caps2_ndims,
                                                  use_bias=True, activation=None, name='recurrent')
                recurrent_state = tf.nn.elu(bottom_up_input + recurrent_input)
        cnn_output = recurrent_state

    # TOPDOWN RNN VERSION // there is no simple RNN class that we can use in tf.Estimators, so instead we repeatedly use the same fc layer with the same parameters
    elif parameters.net_type == 'TOPDOWN_RNN':
        with tf.variable_scope('TOPDOWN_RNN'):
            bottom_layer = tf.nn.elu(flat_conv_output)
            bottom_layer_shape = 1600  # n_units in bottom_layer
            top_layer = tf.layers.dense(bottom_layer, parameters.caps2_ncaps * parameters.caps2_ndims, use_bias=True,
                                        activation=tf.nn.elu, name='top_layer')
        for i in range(parameters.iter_routing - 1):  # -1 because the first iteration is above
            with tf.variable_scope('TOPDOWN_RNN', reuse=tf.AUTO_REUSE):
                top_down_input = tf.layers.dense(top_layer, bottom_layer_shape, use_bias=True, activation=None,
                                                 name='top_down_input')
                bottom_layer = tf.nn.elu(flat_conv_output + top_down_input)
                top_layer = tf.layers.dense(bottom_layer, parameters.caps2_ncaps * parameters.caps2_ndims,
                                            use_bias=True, activation=tf.nn.elu, name='top_layer')
        cnn_output = top_layer

    else:
        raise Exception('Network type not understood, please use "CNN", "LATERAL_RNN OR TOPDOWN_RNN '
                        'for parameters.net_type. You entered {}.'.format(parameters.net_type))

    # print(tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES))  # uncomment to check that recurrent connections use the same weights at each iteration, i.e., it is a recurrent net.

    #####################################################################
    #     Create masked decoder input and decode target orientation     #
    #####################################################################

    with tf.name_scope('2_target_ori_acuity'):
        targori_pred, targori_loss, targori_accuracy = compute_orientation_loss_cnn(cnn_output, targori_label, parameters, is_training)
        targori_loss *= parameters.alpha_targori
        tf.summary.scalar('targori_loss', targori_loss)
        tf.summary.scalar('targori_accuracy', targori_accuracy)

    ##########################################
    #            Prediction mode:            #
    ##########################################
    if mode == tf.estimator.ModeKeys.PREDICT:
        targori_accuracy = compute_accuracy(targori_label[:params['trials']], targori_pred[:params['trials']])
        predictions = {'targori_accuracy': tf.ones(shape=batch_size) * targori_accuracy}  # need batch_size items to return.
        spec = tf.estimator.EstimatorSpec(mode=mode, predictions=predictions)

    ##########################################
    #       Train or Evaluation mode:        #
    ##########################################
    else:        

        with tf.name_scope('3_shape_loss'):
            shape_loss = compute_shape_loss_cnn(cnn_output, type_label, parameters, is_training)
            shape_loss = parameters.alpha_margin * shape_loss
            tf.summary.scalar('shape_loss', shape_loss)

        with tf.name_scope('4_sidesize'):
            if parameters.decode_sidesize:
                sidesize_loss, sidesize_accuracy = compute_sidesize_loss(cnn_output, sidesize_label, parameters, is_training)
                sidesize_loss *= parameters.alpha_sidesize
            else:
                sidesize_loss, sidesize_accuracy = 0., 0.
            tf.summary.scalar('sidesize_loss', sidesize_loss)
            tf.summary.scalar('sidesize_accuracy', sidesize_accuracy)

        with tf.name_scope('5_location'):
            if parameters.decode_location:
                xpos_loss, ypos_loss = compute_location_loss(cnn_output, xpos_label, ypos_label, parameters, is_training)
                xpos_loss = parameters.alpha_xpos_loss * xpos_loss
                ypos_loss = parameters.alpha_ypos_loss * ypos_loss
            else:
                xpos_loss = 0.
                ypos_loss = 0.

            location_loss = xpos_loss + ypos_loss
            tf.summary.scalar('xpos_loss', xpos_loss)
            tf.summary.scalar('ypos_loss', ypos_loss)

        with tf.name_scope('6_n_elements'):
            if parameters.decode_n_elements:
                n_elements_loss, n_elements_accuracy = compute_n_elements_loss(cnn_output, n_elements_label, parameters, is_training)
                n_elements_loss *= parameters.alpha_n_elements
            else:
                n_elements_loss, n_elements_accuracy = 0., 0.
            tf.summary.scalar('n_elements_loss', n_elements_loss)
            tf.summary.scalar('n_elements_accuracy', n_elements_accuracy)

        with tf.name_scope('7_reconstruction'):
            if parameters.reconstruct:
                if parameters.separate_images:  # in this case, we compute reconstructions from each capsule, sum errors and show them in different colors
                    raise Exception('This network has no capsule, so cannot deal with separate images. Please set parameters.separate_images to False.')

                else:
                    # Create decoder outputs for shape_1 images batch
                    reconstruction = compute_reconstruction(cnn_output, parameters, is_training, conv_output_sizes)
                    # Calculate reconstruction loss for shape_1 images batch
                    reconstruction_loss = compute_reconstruction_loss(full_images, reconstruction, parameters)
                    reconstruction_loss *= parameters.alpha_reconstruction
                    tf.summary.scalar('reconstruction_loss', reconstruction_loss)
                    reconstruction_img = tf.reshape(reconstruction, [batch_size, parameters.im_size[0], parameters.im_size[1], 1], name='shape_1_img_reconstructed')
                    tf.summary.image('decoder_output_img', reconstruction_img, plot_n_images)

            else:
                reconstruction_loss = 0.0


    ##########################################
    #              Final loss                #
    ##########################################
        final_loss = tf.add_n([shape_loss, targori_loss, sidesize_loss, location_loss, reconstruction_loss], name='final_loss')


    ##########################################
    #        Training operations             #
    ##########################################
        # The following is needed due to how tf.layers.batch_normalzation works:
        update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
        with tf.control_dependencies(update_ops):
            # learning_rate = tf.train.cosine_decay_restarts(parameters.learning_rate, tf.train.get_global_step(),
            #                                                parameters.learning_rate_decay_steps, name='learning_rate')
            learning_rate = tf.train.cosine_decay_restarts(parameters.learning_rate, tf.train.get_global_step(),
                                                           parameters.learning_rate_decay_steps, alpha=0.4, name='learning_rate')
            optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
            train_op = optimizer.minimize(loss=final_loss, global_step=tf.train.get_global_step(), name='train_op')
            tf.summary.scalar('learning_rate', learning_rate)
        
        # write summaries during evaluation
        eval_summary_hook = tf.train.SummarySaverHook(save_steps=100,
                                                      output_dir=log_dir + '/eval',
                                                      summary_op=tf.summary.merge_all())
        
        # Wrap all of this in an EstimatorSpec.
        # print_hook = tf.train.LoggingTensorHook({"type:": type_label, "targori:": targori_label, "sidesize:": sidesize_label, "xpos:": xpos_label, "ypos:": ypos_label}, every_n_iter=10)
        spec = tf.estimator.EstimatorSpec(
            mode=mode,
            loss=final_loss,
            train_op=train_op,
            eval_metric_ops={},
            # training_hooks=[print_hook],
            evaluation_hooks=[eval_summary_hook])
    
    return spec
