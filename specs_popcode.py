#########################
### Population coding ###
#########################
import numpy as np
import matplotlib.pyplot as plt
from specs_genetic import *
from scipy.optimize import curve_fit
# from RegscorePy import bic


poolRange   = 3.7  # in mm (cortical distances)
poolThresh  = 0.0  # below this value, pooling weights are set to 0.0
def computePopCodePerformance(stim, poolMap, doSegment, plot=False, nTrials=nTrials):

    # Model parameters
    barH       = 0.43                                   # bar height, in degrees
    barW       = 0.09                                   # bar width,  in degrees
    barC       = 30.0                                   # bar contrast 30.0
    alphaC     = barH*barW*barC                         # bar contrast times bar area, in degrees squared
    targOriAmp = 5.0 if doSegment else 12.0             # in degrees, 10.0 for popcode, 5.0 for popart (dense displays)
    targOriAmp = 5.0 if np.abs(stim).sum() < 10 else targOriAmp  # 5.0 for sparse displays, for both popcode and popart
    baseRate   = 5.0                                    # in spikes/s
    nOris      = 90                                     # resolution = (180 degrees)/nOris ; odd number to force decision
    allOris    = np.linspace(-90+180/nOris, 90, nOris)  # in degrees
    oriSpace   = allOris*np.pi/90.0                     # in "2-radians" (for periodicity of tuning curves)
    tuneWidth  = 15.0*np.pi/90.0                        # in "2-radians" (constant used in tuning curves) 15.0
    tuneCurves = tuningCurves(tuneWidth, oriSpace)      # how well neurons respond to different orientations
    gMax       = 90                                     # maximum firing rate of any neuron (in spikes/s) 90
    gain       = gMax*(1.0 - 1.16/(1.0 + np.exp(1.5*(alphaC-1.2))))
    choiceType = 'max'                                  # 'max' or 'evidence'
    priorType  = 'square'                               # 'none', 'square' or 'gaussian'
    prior      = generatePrior(priorType, len(oriSpace))

    # Use by segmentation for popart
    if doSegment:
        from specs_laminart import groupStim, createImages
        barSz     = 7
        gapSz     = 3
        pchSz     = 2*gapSz+barSz
        stim[targPos[0], targPos[1]] = 1  # code for vertical in the Laminart model
        groupList = groupStim(stim)
        stimImage, groupImage = createImages(stim, groupList, targPos, barSz, gapSz, pchSz, plot=False)

    # Different trials for the same stimulus
    t0, t1    = targPos
    nCorrects = 0
    for n in range(nTrials):

        # Segment stimulus (or not) and add target orientation
        stim[t0,t1] = 2
        if doSegment:
            thisStim = segmentStimWithLaminart(stim, groupList, groupImage, stimImage, barSz, gapSz, pchSz, plot)
        else:
            thisStim = stim
        targOri = targOriAmp*np.random.RandomState().choice(a=[1.0, -1.0])

        # Encoding, pooling and decoding
        encodings = encode(thisStim, targOri, gain, alphaC, tuneCurves, baseRate, oriSpace)
        poolings  = np.tensordot(poolMap, encodings, axes=((2,3), (0,1)))
        decoding  = decode(poolings[t0, t1], alphaC, choiceType, priorType, oriSpace)

        # Decision process (target orientation)
        decodedOri = generateResponse(choiceType, oriSpace, decoding, prior)
        if np.sign(decodedOri) == np.sign(targOri):
            nCorrects += 1

        # Plot things (or not)
        if plot:
            plotThingsPopCode(stim, thisStim, poolMap, decodedOri, targOri, poolings[t0, t1], decoding, prior, oriSpace)

    # Return performance of this stimulus
    return nCorrects/nTrials


def polarCoordinates(i, j):

    iTrue = i-(yDim-1)/2                             # fix point aligned with middle row
    jTrue = j+1                                      # fix point shifted by 1 slot wrt 1st column
    ecc   = slotSize*(np.sqrt(iTrue**2 + jTrue**2))  # in degrees of visual angle
    phi   = np.arctan2(iTrue, jTrue)                 # in radians

    return (ecc, phi)


def corticalCoordinates(ecc, phi):

    alpha = 1.0/np.cosh(phi)**(1.0/np.cosh(np.log(ecc/0.77)*0.76)*0.18)
    return 19.2*np.log(ecc*np.exp(1j*phi*alpha)+0.77)


def tuningCurves(tuneWidth, oriSpace):

    nOris  = len(oriSpace)
    curves = np.zeros((nOris, nOris))
    for i, ori in enumerate(oriSpace):
        curves[i] = np.exp((np.cos(oriSpace-ori)-1.0) / (2*tuneWidth**2))

    return curves  # curves[i] is the tuning curve for ori i


def elemDistr(ecc, ori, alphaC, oriSpace):

    # ecc and ori are in degrees, sig and oriSpace are in "2-radians"
    sig = 0.4*(ecc + 2.5)/np.sqrt(alphaC)*np.pi/90.0
    kap = min(sig**(-2), 700)  # inf values in dis.sum() if kap > 700
    ori = ori*np.pi/90         # may be np.random.normal(ori*np.pi/90.0, sig) in the paper but like that in their code
    dis = np.exp(kap*np.cos(oriSpace-ori))

    return dis/dis.sum()  # np.exp(kap*np.cos(oriSpace-ori))/(2*np.pi*np.i0(kap))


def generatePoolMap(poolRange, poolThresh, easyFactor=0.5):

    poolMap = np.zeros((yDim, xDim, yDim, xDim))
    for i in range(yDim):
        for j in range(xDim):
            for i2 in range(yDim):
                for j2 in range(xDim):

                    (ecc , phi ) = polarCoordinates(i , j )
                    (ecc2, phi2) = polarCoordinates(i2, j2)
                    cortCoord    = corticalCoordinates(ecc , phi )
                    cortCoord2   = corticalCoordinates(ecc2, phi2)
                    sigRad       = poolRange
                    sigTan       = poolRange  # /np.sqrt(2)?  # /2?
                    dRad         = np.real(cortCoord) - np.real(cortCoord2)
                    dTan         = np.imag(cortCoord) - np.imag(cortCoord2)
                    poolMap[i,j,i2,j2] = np.exp(-(dRad**2/(2*sigRad**2) + dTan**2/(2*sigTan**2)))

    poolMap[poolMap < poolThresh] = 0.0
    poolMap /= easyFactor
    for i in range(yDim):
        for j in range(xDim):
            poolMap[i, j, i, j] *= easyFactor  # direct connection unchanged

    return poolMap


def generatePrior(priorType, nOris):

    prior = np.ones((nOris,))
    if priorType == 'square':
        prior = np.zeros((nOris,))
        prior[1*nOris//4:3*nOris//4] = 1.0
    if priorType == 'gaussian':
        prior = gaussian(nOris, std=nOris/2)

    return prior


def generateResponse(choiceType, oriSpace, activity, prior):

    nOris      = len(activity)
    decodedOri = 0.0
    if choiceType == 'max':
        decodedOri = oriSpace[np.argmax(activity*prior)]
    if choiceType == 'evidence':
        zeroIdx    = int(np.where(oriSpace==0)[0])
        eviL       = (activity*prior)[zeroIdx-(nOris//2-1):zeroIdx         ].sum()
        eviR       = (activity*prior)[zeroIdx+1           :zeroIdx+nOris//2].sum()
        # decodedOri = np.sign(eviR-eviL) if np.abs(eviL-eviR)/(eviL+eviR) > 1e-2 else 0.0
        decodedOri = np.sign(eviR-eviL)
        
    return decodedOri if decodedOri != 0.0 else np.random.RandomState().choice([1,-1])


def vonMisesMix(x, *p):

    vM = np.zeros_like(x)
    n  = 0
    while True:
        try:
            pNew = p[n*3:(n+1)*3]
            vM  += pNew[0]*np.exp(pNew[1]*np.cos(x-pNew[2])) + (0 if len(pNew) <= 3 else pNew[3])
            n   += 1
        except:
            return vM


def encode(stim, targOri, gain, alphaC, tuneCurves, baseRate, oriSpace):

    r = np.zeros((yDim, xDim, len(oriSpace)))
    for i in range(yDim):
        for j in range(xDim):
            ecc, _ = polarCoordinates(i, j)  # in degrees
            ori    = targOri if stim[i,j] == 2 else (90 if stim[i,j] == -1 else 0)
            amp    = gain if stim[i,j] != 0 else 0.0
            distr  = elemDistr(ecc, ori, alphaC, oriSpace)
            r[i,j] = baseRate + np.matmul(distr, amp*tuneCurves)
            
    return np.random.RandomState().poisson(r)


def decode(activity, alphaC, choiceType, priorType, oriSpace):

    # Decoding parameters
    nMax   = 2       # 1 to 3
    nVars  = 3       # 3 or 4
    bicMin = np.inf  # to select best model

    # Initial prior and fitting
    activity    /= 10000*alphaC    # easier to fit
    all_params_0 = [2e-3, 1.0, 0.0, 0.0][:nVars]*nMax
    for n in range(nMax):
        if n%2 != 0:
            all_params_0[n*nVars + 2] = np.pi

    # Try different sums of von Mises distributions
    for n in range(1, nMax+1):

        # Fit the decoding model parameters to the encoding activity
        try:
            bounds   = ([0, 0, -np.pi, 0][:nVars]*n, [1.0, 47.0, np.pi, 0.5][:nVars]*n)
            params_0 = all_params_0[:n*nVars]
            _pOpt, _ = curve_fit(vonMisesMix, oriSpace, activity, p0=params_0, bounds=bounds, maxfev=500)
        except RuntimeError:
            _pOpt = params_0

        # Compute bic and update parameters if the model is better
        _deco = vonMisesMix(oriSpace, *_pOpt)  # line below does bic.bic(activity, _deco, len(_pOpt)) without RegscorePy
        _bic  = len(_deco)*np.log(((activity-_deco)**2).sum()/len(_deco)) + len(_pOpt)*np.log(len(_deco))
        if _bic < bicMin:
            bicMin   = _bic
            pOpt     = _pOpt

    # Return decoded model to the computer
    return vonMisesMix(oriSpace, *_pOpt)


def plotThingsPopCode(stim, segmentedStim, poolMap, decodedOri, targOri, pooling, decoding, prior, oriSpace):

    stim[targPos[0], targPos[1]] = 2
    plt.subplot(221)
    plt.imshow(stim)
    plt.subplot(222)
    plt.imshow(segmentedStim)
    plt.subplot(223)
    plt.imshow(poolMap[targPos[0], targPos[1]])
    plt.subplot(224)
    plt.plot(oriSpace*90/np.pi, pooling,        'b')
    plt.plot(oriSpace*90/np.pi, decoding,       'r')
    plt.plot(oriSpace*90/np.pi, decoding*prior, 'm')
    plt.axvline(x=0, linestyle='--',      color='k')
    plt.axvline(x=decodedOri*90/np.pi,    color='y')
    plt.axvline(x=targOri,                color='c')
    plt.suptitle('Target ori: %1.2f, decoded ori: %1.2f' % (targOri, decodedOri*90/np.pi))
    plt.show()


def segmentStimWithLaminart(stim, groupList, groupImage, stimImage, barSz, gapSz, pchSz, plot=False):

    # Parameters
    segType   = 'around'               # around or uniform
    nSegLayrs = 5                      # number of seg, layers; number of seg. signals is nSegLayrs-1
    pchPerDeg = 1.0/slotSize           # how many patch per degree
    pixPerDeg = pchSz*pchPerDeg        # how many pixel per degree
    bouRadius = 0.5*targEcc*pchPerDeg  # Bouma's radius (half the eccentricity) in patches
    segRadius = 26                     # This corresponds to the size of seg. signals in Francis (2017)

    # Compute groups from the stimulus array
    stim[targPos[0], targPos[1]] = 1
    from specs_laminart import segmentStim
    signalsMap, segLists = segmentStim(groupList, groupImage, stimImage, nSegLayrs, targPos, pchSz, bouRadius, segRadius, segType)

    # Plots the segmented output
    if plot:
        for h in range(nSegLayrs):
            _, segImage = createImages(stim, segLists[h], targPos, barSz, gapSz, pchSz, plot=False)
            if h == 0:
                segImage += signalsMap
            plt.subplot(1, nSegLayrs, h+1)
            plt.imshow(segImage.astype(np.uint8))
        plt.show()

    # Find the segmentation layer that contains the target and create the segmentation output
    segmentedStim = np.zeros(stim.shape, dtype=int)
    for h in range(segLists.shape[0]):
        if segLists[h, targPos[0], targPos[1]] != -1:
            for i in range(yDim):
                for j in range(xDim):

                    # Check whether there was something at position (i,j)
                    if segLists[h,i,j] != -1:
                        segmentedStim[i,j] = stim[i,j]

    # Return the segmented stim to the population coding algorithm
    segmentedStim[targPos[0], targPos[1]] = 2
    return segmentedStim