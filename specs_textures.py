########################################
### Textures performance definitions ###
########################################
import numpy as np
import os
from specs_genetic import *
from scipy.signal import convolve2d

import matlab.engine as engine
import matlab
cwd = os.getcwd()
eng = engine.start_matlab()
eng.addpath(cwd + r'\Textures\textureSynth',   nargout=0)
eng.addpath(cwd + r'\Textures\matlabPyrTools', nargout=0)
def computeTexturesPerformance(stim, plot=False, nTrials=nTrials):

    # Stimulus parameters
    doBouma  = matlab.logical([1])  # 0 or 1: if 1: compute texture on a Bouma-sized patch
    targAmpl = 15.0  # if np.abs(stim).sum() < 100 else 15.0

    # Template match parameters
    offMult  = -0.5  # in interval ]-1.0, 0.0]
    nConvs   =  1    # natural number
    decision = ['L', 'R']

    # Build filters
    filterOn  = np.ones((5, 2))
    filterOff = offMult * filterOn
    filterL   = np.vstack([np.hstack([filterOn, filterOff]), np.hstack([filterOff, filterOn])])
    filterR   = np.vstack([np.hstack([filterOff, filterOn]), np.hstack([filterOn, filterOff])])

    # Set the stimulus array (in stim, {1, 2, 3, 4} are {vert, hori, empty, target})
    this_stim = np.array(stim, dtype=np.int8)
    this_stim[this_stim ==  0] = 3  # empty patch
    this_stim[this_stim == -1] = 2  # hori  patch
    this_stim = matlab.uint8(this_stim.tolist())  # Needed for compatibility with matlab engine
    targEccM  = matlab.double([targEcc ])         # Needed for compatibility with matlab engine
    slotSizeM = matlab.double([slotSize])         # Needed for compatibility with matlab engine

    # Time runs (because it takes long)
    import time
    t0 = time.time()

    # Trial loop (stim: 1=vertical, 2=horizontal, 3=nothing)
    nCorrects = 0
    for n in range(nTrials):

        # ori = targAmpl*(-1)**n                                        # Target orientation [deg]
        ori = targAmpl*np.random.RandomState().choice(a=[1.0, -1.0])  # Target orientation [deg]
        label = 'O'
        if ori > 0:
            label = 'R'
        if ori < 0:
            label = 'L'
        targOri = matlab.double([ori])

        # Matlab function call to generate a texture image
        textImg, _, _ = eng.erikGenerateImage(this_stim, targEccM, slotSizeM, targOri, doBouma, True, nargout=3)

        # Plot things (or not)
        if plot:
            import matplotlib.pyplot as plt
            plt.imshow(textImg)
            plt.title(n)
            plt.show()

        # Core snippet of the template match algorithm
        convL = None
        convR = None
        for c in range(nConvs + 1):
            convL = convolve2d(convL, filterL, mode='valid') if convL is not None else np.array(textImg).copy()
            convR = convolve2d(convR, filterR, mode='valid') if convR is not None else np.array(textImg).copy()

        # Take the max of evidence
        convMaxes = [convL.max(), convR.max()]
        maxDec    = decision[np.argmax(convMaxes)]

        # Collect the hit or fail
        if maxDec == label:
            nCorrects += 1

    # Return proportion correct to the computer
    print('It took %4.2f s to run this stimulus.' % (time.time()-t0))
    return nCorrects/nTrials