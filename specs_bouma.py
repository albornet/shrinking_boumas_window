#########################
### Bouma's law model ###
#########################
from specs_genetic import *
from scipy.optimize import curve_fit

unflankedPerf = 0.96
def computeBoumaPerformance(stim, v_weights, h_weights, doVdB=False, plot=False, nTrials=nTrials):

    # Parameter mapping model results to performance values
    interferenceGain = 1.0 if np.abs(stim).sum() < 10 else 0.3

    # Go through all trials
    nCorrects = 0
    for n in range(nTrials):

        # Sum interferences
        interference = 0.0
        for i in range(yDim):
            for j in range(xDim):

                # Different weights for vertical or horizontal flankers
                if stim[i,j] ==  1:  # V
                    interference += interferenceGain*v_weights[i,j]
                if stim[i,j] == -1:  # H
                    interference += interferenceGain*h_weights[i,j]

        # Compute probability of being correct and roll a dice
        pCorrect = max(unflankedPerf*(1-interference), 0.5)
        if doVdB:                 # use pCorrect instead of giving 12 trial responses
            nCorrects += pCorrect
        else:                     # correct: 1 ; wrong: 0
            nCorrects += np.random.RandomState().choice(a=[1, 0], p=[pCorrect, 1-pCorrect])

    # Plot interaction weights (or not)
    if plot:
        import matplotlib.pyplot as plt
        plt.subplot(1,2,1)
        plt.imshow(v_weights)
        plt.title('Vertical flanker weights')
        plt.subplot(1,2,2)
        plt.imshow(h_weights)
        plt.title('Horizontal flanker weights')
        plt.show()
        
    # Return proportion correct
    return nCorrects/(nTrials)


def fitWeightFunction():

    # Function to fit
    def boumaWeightFunction(d, *p):
        return p[0]*np.exp(-d/p[1])

    # Parameters initialization
    params_0 =  [0.3, 2.0]
    bounds   = ([0.0, 0.1], [0.5, 10.0])

    # Bouma's law results (featuring 4 flankers, either all vertical or all horizontal) from Van der Burg (2016)
    v_performances  = [0.56, 0.65, 0.79, 0.86, 0.90, 0.93, 0.92]
    h_performances  = [0.84, 0.92, 0.97, 0.91, 0.95, 0.97, 0.95]
    v_interferences = [(unflankedPerf-i)/4 for i in v_performances]
    h_interferences = [(unflankedPerf-i)/4 for i in h_performances]

    # Fit a function that takes distances as input and return interferences
    distances   = np.arange(1, 1+len(v_interferences))
    params_v, _ = curve_fit(boumaWeightFunction, distances, v_interferences, p0=params_0, bounds=bounds, maxfev=500)
    params_h, _ = curve_fit(boumaWeightFunction, distances, h_interferences, p0=params_0, bounds=bounds, maxfev=500)

    # Use the fitted parameters to build weight matrices for vertical and horizontal flankers (knowing target position)
    v_weights = np.zeros((yDim, xDim))
    h_weights = np.zeros((yDim, xDim))
    for i in range(yDim):
        for j in range(xDim):
            if not (i == targPos[0] and j == targPos[1]):
                targFlankDist  = np.sqrt(((i-targPos[0])**2 + (j-targPos[1])**2))
                v_weights[i,j] = boumaWeightFunction(targFlankDist, *params_v)
                h_weights[i,j] = boumaWeightFunction(targFlankDist, *params_h)

    # Threshold on weights
    v_weights[v_weights<0.02] = 0.0
    h_weights[h_weights<0.02] = 0.0

    # Return the parameters to the computer
    return v_weights, h_weights